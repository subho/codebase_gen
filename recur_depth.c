#include <stdio.h>

void recur(){
	static int i=0;
	printf("%d\n",i+1);
	i++;
	recur();
}

int main(){
	recur();
	return 0;
}
