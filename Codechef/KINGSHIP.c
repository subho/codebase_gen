#include <stdio.h>
#include <stdlib.h>

#define lli long long

int n;

void swap(int* a, int* b)
{
    int temp;
    temp = *a;
    *a = *b;
    *b = temp;
    return;
}

lli min_cost(int* arr, int sz, int* arr_disp)
{
    lli cost = 0;
    lli cost_calc;
    arr_disp[n-sz] = arr[0];
    if(sz == 1){
        int temp=0;
        for(int i=0;i<n;i++){
            printf("%d\t", arr_disp[i]);
            if(i>0) temp = temp + arr_disp[i]*arr_disp[i-1];
        }
        printf(":: %d\n",temp);
        return 0;
    }

    int* arr_new;
    arr_new = (int*)calloc(sizeof(int),(sz-1));

    for(int i=0;i<(sz-1);i++)
        arr_new[i] = arr[i+1];


    for(int i=1; i<sz; i++){
        swap(&arr_new[0], &arr_new[i-1]);
        cost_calc = arr[0]*arr[i] + min_cost(arr_new, sz-1,arr_disp);
        if(cost == 0) cost = cost_calc;
        else if(cost > cost_calc) cost = cost_calc;
        swap(&arr_new[0], &arr_new[i-1]);
    }
    free(arr_new);
    return cost;
}


int main()
{
    //FILE* file;
    printf("Entering the no.of costs :: ");
    scanf("%d",&n);

    //file = fopen("KINGSHIP.txt","r");
    int* arr;
    arr = (int *)malloc(sizeof(int)*n);

    int* arr_disp;
    arr_disp = (int *)malloc(sizeof(int)*n);

    printf("Enter the costs below:: \n");
    for(int i=0;i<n;i++)
        scanf("%d",&arr[i]);

    printf("Minimum cost is ... %lld\n", min_cost(arr,n,arr_disp));

    return 0;

}
