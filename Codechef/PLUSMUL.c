#include <stdio.h>
#include <stdlib.h>

#define MAX_ELEMS 1000

int main(){
    int T;
    scanf("%d", &T);

    while(T--){
        int num;
        scanf("%d",&num);
        int* init_arr = (int*)malloc(sizeof(int)*num);

        for(int i=0;i<num;i++)
            scanf("%d",init_arr+i);

        int (*calc_arr)[2] = (int **)malloc(sizeof(int)*num*2);

        for(int i=0; i<num; i++){
            calc_arr[i][0] = 0;
            calc_arr[i][1] = 0;
        }

        for(int j=0; j<num; j++)
            printf("%d   %d\n", calc_arr[j][0],calc_arr[j][1]);


        int pow = 1;
        calc_arr[0][0] = init_arr[0];
        calc_arr[0][1] = init_arr[0];
        for(int i=1; i<num;i++){
            int to_add;
            int to_mul;
            int temp;
            if(i>=2) pow = pow*2;
            for(int j=0; j<=i; j++){
                if(i < 2 || i-j<2) to_mul = 1;
                else to_mul = 2;
                if(j == 0){
                    if(to_mul != 1)
                        calc_arr[j][0] = to_mul*calc_arr[j][0] - calc_arr[j][1];
                    to_add = init_arr[i]*pow;
                    calc_arr[j][0] += to_add;
                }
                else {
                    temp = to_add;
                    if(to_mul != 1)
                        calc_arr[j][0] = to_mul*calc_arr[j][0] - calc_arr[j][1];
                    to_add = init_arr[i]*calc_arr[j-1][1];
                    calc_arr[j][0] += to_add;
                    calc_arr[j-1][1] = temp;
                }
                calc_arr[i][1] = temp;
                for(int j=0; j<num; j++)
                    printf("%d   %d\n", calc_arr[j][0],calc_arr[j][1]);
            }
            printf("\n\n");
        }

        int sum = 0;
        for(int i=0; i<num; i++){
            sum = sum + calc_arr[i][0];
        }
        printf("sum = %d\n",sum);
    }
}
