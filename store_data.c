#include <stdio.h>
#include <stdlib.h>

#define MAX_AGE 100
#define MAX_CHAR 20
#define MAX_S_AGE 5

//assumptions : Not more than 5 people 
//			    will have the same age
//			  : the name will not be 
//				20 chars in length

int main()
{
	char names[MAX_AGE][MAX_S_AGE][MAX_CHAR] = {'\0'};
	int num;
	
	printf("Enter the number of names you want to enter: ");
	scanf("%d[^\n]", &num);
	
	printf("Now enter the names in AGE NAME format::\n");
	for(int i=0; i<num;i++){
		int age;
		scanf("%d[^ ]",&age);
		int j;
		for(j=0;names[age][j][0]!='\0'&&j<5;j++)
			continue;
		if(j==5) error("Assumption 1 failed");
		scanf("%s[^\n]",&names[age][j]);
	}
	
	//print the names here
	for(int i=0;i<MAX_AGE;i++)
		for(int j=0;names[i][j][0]!='\0';j++)
			printf("%d.....%s\n",i,names[i][j]);
	
	return 0;
}
	
		
	
	
	
	
