#include <stdio.h>
#include <stdlib.h>

#define MAX_ELEMENTS 20
#define SENTINEL -1

struct mtrx_trm{
    int row;
    int col;
    float val;
};

struct SPARSE_MAT{
    struct mtrx_trm term[2*MAX_ELEMENTS];
    int num_terms;
    int rows;
    int cols;
};

struct SPARSE_MAT create(int rows, int cols){
    struct SPARSE_MAT M;
    M.num_terms = 0;
    M.rows = rows;
    M.cols = cols;

    for(int i=0;i<MAX_ELEMENTS;i++){
        M.term[i].row = 0;
        M.term[i].col = 0;
        M.term[i].val = 0;
    }
    if(rows*cols > MAX_ELEMENTS)
        printf("The max number of elements in this matrix can be %d\n",MAX_ELEMENTS);
    else
        printf("The max number of elements in this matrix can be %d\n",rows*cols);
    return M;
}

void store_val(struct SPARSE_MAT* M, int row, int col, float val){
    (M->term[M->num_terms]).row = row;
    (M->term[M->num_terms]).col = col;
    (M->term[M->num_terms]).val = val;
    M->num_terms += 1;
    return;
}

void sort_row_major(struct SPARSE_MAT* M){

    int num_terms = M->num_terms;
    int cols = M->cols;
    for(int i=0;i<num_terms;i++){
        int val_i = M->term[i].row*cols + M->term[i].col;
        for(int j = i+1;j<num_terms;j++){
            int val_j = M->term[j].row*cols + M->term[j].col;
            if(val_i>val_j){
                struct mtrx_trm m;
                m = M->term[i];
                M->term[i] = M->term[j];
                M->term[j] = m;
            }
        }
    }

        return;
}

void sort_col_major(struct SPARSE_MAT* M){
    int num_terms = M->num_terms;

    int rows = M->rows;
    for(int i=0;i<num_terms;i++){
        int val_i = M->term[i].col*rows + M->term[i].row;
        for(int j= i+1;j<num_terms;j++){
            int val_j = M->term[i].col*rows + M->term[i].row;
            if(val_i>val_j){
                struct mtrx_trm m;
                m = M->term[i];
                M->term[i] = M->term[j];
                M->term[j] = m;
            }
        }
    }
    return;
}

int find_index(struct SPARSE_MAT M, int row, int col){
    for(int i=0; i<M.num_terms; i++){
        if(M.term[i].row == row && M.term[i].col == col)
            return i;
    }

    return SENTINEL;
}

struct SPARSE_MAT mul(struct SPARSE_MAT M1, struct SPARSE_MAT M2){
    if(M1.cols != M2.rows) perror("This multiplication ain't possible\n");

    struct SPARSE_MAT M;
    M = create(M1.rows, M2.cols);

    sort_col_major(&M1);
    sort_row_major(&M2);

    for(int i=0; i<M1.num_terms; i++){
        for(int j=0; j<M2.num_terms; j++){
            if(M2.term[j].row == M1.term[i].col){
                int mul = M2.term[j].val*M1.term[i].val;
                int found;
                if((found=find_index(M,M1.term[i].row,M2.term[j].col)) != SENTINEL)
                    M.term[found].val += mul;
                else store_val(&M,M1.term[i].row,M2.term[j].col,mul);
            }
        }
    }

    return M;

}

struct SPARSE_MAT add(struct SPARSE_MAT M1, struct SPARSE_MAT M2){
    struct SPARSE_MAT M;

    if(M1.rows != M2.rows || M1.cols != M2.cols){
        M = create(0,0);
        return M;
    }

    M = create(M1.rows, M1.cols);

    int i_M1 = 0;
    int i_M2 = 0;

    sort_row_major(&M1);
    sort_row_major(&M2);

    while(i_M1 < M1.num_terms || i_M2 < M2.num_terms){
        if(i_M1 == M1.num_terms){
            store_val(&M,M2.term[i_M2].row,M2.term[i_M2].col,M2.term[i_M2].val);
            i_M2 += 1;
        }
        else if(i_M2 == M2.num_terms){
            store_val(&M,M1.term[i_M1].row,M1.term[i_M1].col,M1.term[i_M1].val);
            i_M1 += 1;
        }
        else if(M1.term[i_M1].row == M2.term[i_M2].row){
            if(M1.term[i_M1].col == M2.term[i_M2].col){
                float val = M1.term[i_M1].val + M2.term[i_M2].val;
                store_val(&M,M1.term[i_M1].row,M1.term[i_M1].col,val);
                i_M1 += 1;
                i_M2 += 1;
            }
            else if(M1.term[i_M1].col < M2.term[i_M2].col){
                store_val(&M,M1.term[i_M1].row,M1.term[i_M1].col,M1.term[i_M1].val);
                i_M1++;
            }
            else {
                store_val(&M,M2.term[i_M2].row,M2.term[i_M2].col,M2.term[i_M2].val);
                i_M2 += 1;
            }

        }
        else if(M1.term[i_M1].row < M2.term[i_M2].row){
            store_val(&M,M1.term[i_M1].row,M1.term[i_M1].col,M1.term[i_M1].val);
            i_M1 += 1;
        }
        else{
            store_val(&M,M2.term[i_M2].row,M2.term[i_M2].col,M2.term[i_M2].val);
            i_M2 += 1;
        }
    }

    if(M.num_terms > MAX_ELEMENTS){
        printf("ABORT!! BUFFER EXCEEDED IN ADD\n");
        exit(1);
    }

    return M;
}

struct SPARSE_MAT const_mul(struct SPARSE_MAT M, float c){
    for(int i=0; i<M.num_terms; i++)
        M.term[i].val *= c;

    return M;
}

void transpose(struct SPARSE_MAT *M){
    int swap = 0 ;
    for(int i=0;i<M->num_terms;i++){
        swap = M->term[i].row;
        M->term[i].row = M->term[i].col;
        M->term[i].col = swap;
    }

    return;
}

void display(struct SPARSE_MAT M){
    while(M.num_terms--){
        printf("row = %d\t|",M.term[M.num_terms].row);
        printf("col = %d\t|",M.term[M.num_terms].col);
        printf("val = %f\n",M.term[M.num_terms].val);
    }
    printf("**********************************************\n");
    return;
}


int main(){
    struct SPARSE_MAT M1, M2;
    int rows, cols, num;

    printf("Enter the rows, columnns for matrix M1 :: ");
    scanf("%d",&rows);
    scanf("%d",&cols);
    M1 = create(rows,cols);

    printf("Enter the number of elements for matrix M1 :: ");
    scanf("%d",&num);

    if(num > rows*cols) {
        printf("Math - illiterate! Exiting!\n");
        exit(1);
    }

    printf("Enter the elements in row,column,val format for M1 :: \n");

    int row;
    int col;
    float val;
    while(num--){
        scanf("%d",&row);
        scanf("%d",&col);
        scanf("%f",&val);
        store_val(&M1, row, col, val);
    }

    printf("Enter the rows, columnns for matrix M1 :: ");
    scanf("%d",&rows);
    scanf("%d",&cols);
    M2 = create(rows,cols);

    printf("Enter the number of elements for matrix M2 :: ");
    scanf("%d",&num);

    if(num > rows*cols) {
        printf("Math - illiterate! Exiting!\n");
        exit(1);
    }

    printf("Enter the elements in row,column,val format for M2 :: \n");

    while(num--){
        scanf("%d",&row);
        scanf("%d",&col);
        scanf("%f",&val);
        store_val(&M2,row,col,val);
    }

    struct SPARSE_MAT M;
    M = mul(M1,M2);

    display(M);

    return 0;
}
