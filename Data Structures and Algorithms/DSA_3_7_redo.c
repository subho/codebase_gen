#include <stdio.h>
#include <stdlib.h>
#include <stdint.h>

#define MIN_BLOCK_SIZE 200
#define TRUE           1
#define FALSE          0
#define S_OFFSET       -1

typedef struct block_tag{
    struct block_tag* prev;
    uint16_t initSize;
    char* startUserSpace;
    char* freeSpaceStart;
    struct block_tag* next;
}block;

typedef struct block_head{
    block* head;
    uint16_t numBlocks;
}blockHeader;

blockHeader usedBlocks;
blockHeader freeBlocks;

void initBlockHeader(){
    usedBlocks.head = NULL;
    usedBlocks.numBlocks = 0;

    freeBlocks.head = NULL;
    freeBlocks.numBlocks = 0;
}

block* createBlock(uint16_t size){
    int sizeToUuse;
    if(size < MIN_BLOCK_SIZE)
        sizeToUuse = MIN_BLOCK_SIZE;
    else sizeToUuse = size;

    block* b = (block*)malloc(sizeof(block)+sizeToUuse);
    b->prev  = NULL;
    b->next  = NULL;
    b->initSize = sizeToUuse;
    b->startUserSpace = (char*)b+sizeof(block);
    b->freeSpaceStart = (char*)b+sizeof(block);
    return b;
}

//Always insert a newblock after the head
void insertBlock(blockHeader* b, block* bl){
    if(b->head == NULL){
        b->head = bl;
        bl->prev = bl;
        bl->next = bl;
    }
    else{
        bl->next = b->head;
        bl->prev = b->head->prev;
        bl->prev->next = bl;
        bl->next->prev = bl;
    }
    b->numBlocks++;
}

void deleteBlock(blockHeader* b, block* target){
    //If the target is the only node, then update head in that block
    if(target->next == target->prev)
        b->head = NULL;
    else{
        //If the target is the head, then update the head before operation
        if(target == b->head)
            b->head = target->prev;

        //Fix the links before removing the elements
        target->prev->next = target->next;
        target->next->prev = target->prev;
    }

    //Free the block after all the processing done above and decrement the
    //numBlocks.
    free(target);
    b->numBlocks--;
}

//Remove block from the list and make it free
void removeBlock(blockHeader* b, block* target){
    if(target->next == target->prev)
        b->head = NULL;
    else{
        if(target == b->head)
            b->head = target->prev;

        //Fix the links before removing the elements
        target->prev->next = target->next;
        target->next->prev = target->prev;
    }
    b->numBlocks--;
}

//Write the header footer information of free user space
void writeFreeHF(const char* startAddress, uint16_t size, short int prev, short int next){
    uint8_t* Flag_start = (uint8_t*)startAddress;
    uint16_t* Size_start = (uint16_t*)(Flag_start+1);
    short int* Prev = (short int*)(Size_start+1);
    short int* Next = Prev+1;
    uint16_t* Size_end = (uint16_t*)(startAddress+size-sizeof(uint16_t)-sizeof(uint8_t));
    uint8_t* Flag_end = (uint8_t*)(Size_end+1);

    *Flag_end = *Flag_start = FALSE;
    *Size_end = *Size_start = size;
    *Prev = prev;
    *Next = next;
}

//Write header footer information of used up user Space
void writeUsedHF(char* startUserSpace, char* startAddress, uint16_t size){
    uint8_t* Flag_start = (uint8_t*)startAddress;
    uint16_t* Size_start = (uint16_t*)(Flag_start+1);
    uint16_t* Offset     = Size_start + 1;
    uint16_t* Size_end   = (uint16_t*)(startAddress+size-sizeof(uint16_t)-sizeof(uint8_t));
    uint8_t* Flag_end = (uint8_t *)(Size_end+1);

    *Flag_end = *Flag_start = TRUE;
    *Size_start = *Size_end = size;
    *Offset = startAddress - startUserSpace;
}

//Find memory in a block for allocation with specified Size
//Here we'll try to find the best fit for the data
char* findMemBlk(const block* b, const uint16_t size){
    if(b->freeSpaceStart == NULL) return NULL;
    char* startAddress = b->freeSpaceStart;
    char* addressToReturn = NULL;
    uint16_t size_free_curr = 0;
    uint16_t size_free_prev = 0;
    while(startAddress >= (b->startUserSpace)){
        //the size instruction is present after
        size_free_curr = *(uint16_t*)(startAddress+sizeof(uint8_t));
        if(size_free_curr >=size){
            if(size_free_prev>size_free_curr || size_free_prev == 0){
                size_free_prev = size_free_curr;
                addressToReturn = startAddress;
            }
        }
        startAddress = b->startUserSpace + *(short int*)(startAddress + sizeof(uint8_t) + sizeof(uint16_t) + sizeof(short int));
    }
    return addressToReturn;
}

char* resizeSpace(block* b, char* startAddress, uint16_t size){
    uint16_t* freeSize = (uint16_t*)(startAddress + sizeof(uint8_t));
    uint16_t spaceLeft = (*freeSize) - size;

    uint16_t minFreeSize = (2*sizeof(uint8_t) + 2*sizeof(uint16_t) +2*sizeof(short int));

    if(spaceLeft<minFreeSize){
        if(startAddress == b->freeSpaceStart){
            short int *next = (short int*)(startAddress + sizeof(uint8_t) + sizeof(uint16_t) + sizeof(short int));
            if(*next != S_OFFSET){
                b->freeSpaceStart = b->startUserSpace + (*next);
                short int *prvNextSpace = (short int*)(b->freeSpaceStart + sizeof(uint16_t) + sizeof(uint8_t));
                *prvNextSpace = S_OFFSET;
            } else{
                b->freeSpaceStart = NULL;
            }
        } else{
            short int *prev = (short int*)(startAddress + sizeof(uint8_t) + sizeof(uint16_t));
            short int *next = prev + 1;

            char* startPrevSpace = b->startUserSpace+ (*prev);
            short int *nxtPrevSpace = (short int*)(startPrevSpace + sizeof(uint16_t) + sizeof(uint8_t) + sizeof(short int));
            *nxtPrevSpace = *next;

            if(*next != S_OFFSET){
                char* startNextSpace = b->startUserSpace + (*next);
                short int *prvNextSpace = (short int*)(startNextSpace + sizeof(uint16_t) + sizeof(uint8_t));
                *prvNextSpace = *prev;
            }
        }
        writeUsedHF(b->startUserSpace,startAddress,*freeSize);
        return startAddress + sizeof(uint8_t) + sizeof(uint16_t) + sizeof(uint16_t);
    } else{
        short int* prev = (short int*)(startAddress + sizeof(uint16_t) + sizeof(uint8_t));
        short int* next = prev + 1;

        writeFreeHF(startAddress,spaceLeft,*prev,*next);
        writeUsedHF(b->startUserSpace,startAddress+spaceLeft, size);

        return startAddress+spaceLeft+sizeof(uint16_t)+sizeof(uint8_t) + sizeof(uint16_t);
    }
}

//Here we're allocating the memory to user
char* allocate_mem(uint16_t size){
    uint8_t sizeDiff = 2*sizeof(short int) - sizeof(uint16_t);
    size = size>sizeDiff?size:sizeDiff;
    size = size+3*sizeof(uint16_t)+2*sizeof(uint8_t);

    block* blockStart = usedBlocks.head;
    char*  addressToReturn = NULL;
    for(int i=0; i<usedBlocks.numBlocks; i++){
        if((addressToReturn=findMemBlk(blockStart, size)))
            break;
        blockStart = blockStart->next;
    }
    if(addressToReturn == NULL){
        if(!freeBlocks.head){
            insertBlock(&usedBlocks,(blockStart = createBlock(size)));
            //Initliazing the block
            writeFreeHF(blockStart->startUserSpace,blockStart->initSize,S_OFFSET,S_OFFSET);
        }
        else{
            removeBlock(&freeBlocks,(blockStart = freeBlocks.head));
            insertBlock(&usedBlocks,blockStart);
        }
        addressToReturn = blockStart->freeSpaceStart;
    }

    addressToReturn = resizeSpace(blockStart,addressToReturn,size);
    return addressToReturn;
}

void dealloc_mem(void* userPointer){
    char* startAddress = (char*)userPointer - sizeof(uint8_t) - 2*sizeof(uint16_t);
    uint16_t Offset = *(uint16_t*)(startAddress + sizeof(uint16_t) + sizeof(uint8_t));
    int free_block_present = FALSE;
    if(Offset != 0){
        if(*(uint8_t*)(startAddress-sizeof(uint8_t)) == FALSE)
            free_block_present = TRUE;
    }

    if(free_block_present){
        char* startFreeAdd = startAddress - *(uint16_t*)(startAddress - sizeof(uint16_t) - sizeof(uint8_t));
        uint16_t Size = *(uint16_t*)(startFreeAdd + sizeof(uint8_t));
        short int Prev = *(short int*)(startFreeAdd + sizeof(uint8_t) + sizeof(uint16_t));
        short int Next = *(short int*)(startFreeAdd + sizeof(uint8_t)  + sizeof(uint16_t) + sizeof(short int));

        uint16_t usedSize = *(uint16_t*)(startAddress + sizeof(uint8_t));
        writeFreeHF(startFreeAdd,Size+usedSize,Prev,Next);
    }
    else{
        uint16_t Size = *(uint16_t*)(startAddress + sizeof(uint8_t));
        short int Prev = -1;

        block* b = (block*)(startAddress - *(uint16_t*)(startAddress + sizeof(uint8_t) + sizeof(uint16_t)) - sizeof(block));
        char* nxtFreeSpace = b->freeSpaceStart;
        short Next = S_OFFSET;
        if(nxtFreeSpace){
            short int* nxtFreePrev  = (short int*)(nxtFreeSpace + sizeof(uint8_t) + sizeof(uint16_t));
            *nxtFreePrev = startAddress - b->startUserSpace;
            Next = nxtFreeSpace - b->startUserSpace;
        }
        b->freeSpaceStart = startAddress;
        writeFreeHF(startAddress,Size,Prev,Next);
    }
}

//Visualize block
void vis_block(const block* b){
    char* start = b->startUserSpace;
    uint16_t offset = 0;
    printf("=========================================================\n");
    printf("Prev == %p\n",b->prev);
    printf("Size == %d\n", b->initSize);
    printf("User Space start address == %p\n", b->startUserSpace);
    printf("Free Space start address == %p\n", b->freeSpaceStart);
    printf("Next == %p\n",b->prev);

    while(offset<b->initSize){
        uint8_t* Flag = (uint8_t*)(start + offset);
        if((*Flag) == FALSE){
            printf("|F %d F|", *(uint16_t*)(Flag+1));
        }else{
            printf("|U %d U|", *(uint16_t*)(Flag+1));
        }
        offset = offset + *(uint16_t *)(Flag + 1);
    }
    printf("\n");
    printf("=========================================================\n");
}

int main(){
    int* var1 = (int*)allocate_mem(sizeof(int));
    vis_block(usedBlocks.head);
    int* var2 = (int*)allocate_mem(sizeof(int));
    vis_block(usedBlocks.head);
    dealloc_mem(var1);
    vis_block(usedBlocks.head);
    int* var3 = (int*)allocate_mem(sizeof(int)*100);
    vis_block(usedBlocks.head);
    vis_block(usedBlocks.head->next);
    dealloc_mem(var2);
    vis_block(usedBlocks.head);
    dealloc_mem(var3);
    vis_block(usedBlocks.head);
    vis_block(usedBlocks.head->next);
    return 0;
}
