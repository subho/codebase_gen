#include <stdio.h>
#include <stdlib.h>

#define SENTINEL_VAL 0

typedef struct SM_elem_tag{
    int row;
    int col;
    float val;
    struct SM_elem_tag* next_row;
    struct SM_elem_tag* next_col;
    struct SM_elem_tag* prev_row;
    struct SM_elem_tag* prev_col;
} SM_elem;

SM_elem* create_head(int rows, int cols){
    SM_elem* head;
    head = (SM_elem*)malloc(sizeof(SM_elem));

    head->row = rows;
    head->col = cols;
    head->val = SENTINEL_VAL;

    head->next_col = NULL;
    head->next_row = NULL;
    head->prev_col = NULL;
    head->prev_row = NULL;

    return head;
}

SM_elem* find_row(SM_elem* head, int row){
    head = head->next_row;

    while(head != NULL){
        if(head->row == row)
            return head;
        else head = head->next_row;
    }

    return NULL;
}

SM_elem* find_col(SM_elem* head, int col){
    head = head->next_col;

    while(head != NULL){
        if(head->col == col)
            return head;
        else head = head->next_col;
    }

    return NULL;
}

SM_elem* find_elem(SM_elem* head, int row, int col){
    SM_elem* elem_address = NULL;
    SM_elem* row_address = find_row(head, row);

    if(row_address == NULL) return NULL;
    else elem_address = find_col(row_address, col);

    return elem_address;
}

SM_elem* add_row(SM_elem* head, int row){
    SM_elem* elem;
    elem = (SM_elem*)malloc(sizeof(SM_elem));

    elem->val = SENTINEL_VAL;
    elem->row = row;
    elem->col = 0;


    if(head->next_row == NULL){
        elem->next_row = NULL;
        elem->next_col = NULL;
        elem->prev_row = head;
        elem->prev_col = NULL;
        head->next_row = elem;
        return elem;
    }
    else{
        head = head->next_row;
        while(head->row<row){
            if(head->next_row != NULL)
                head = head->next_row;
            else{
                elem->next_row = NULL;
                elem->next_col = NULL;
                elem->prev_row = head;
                elem->prev_col = NULL;
                head->next_row = elem;
                return elem;
            }
        }

        head = head->prev_row;
        elem->next_row = head->next_row;
        elem->next_col = NULL;
        elem->prev_row = head;
        elem->prev_col = NULL;
        head->next_row = elem;
        return elem;
    }
}

SM_elem* add_col(SM_elem* head, int col){
    SM_elem* elem;
    elem = (SM_elem*)malloc(sizeof(SM_elem));

    elem->val = SENTINEL_VAL;
    elem->row = 0;
    elem->col = col;

    if(head->next_col == NULL){
        elem->next_row = NULL;
        elem->next_col = NULL;
        elem->prev_col = head;
        elem->prev_row = NULL;
        head->next_col = elem;
        return elem;
    }
    else{
        head = head->next_col;
        while(head->col<col){
            if(head->next_col != NULL)
                head = head->next_col;
            else{
                elem->next_row = NULL;
                elem->next_col = NULL;
                elem->prev_col = head;
                elem->prev_row = NULL;
                head->next_col = elem;
                return elem;
            }
        }

        head = head->prev_col;
        elem->next_row = NULL;
        elem->next_col = head->next_col;
        elem->prev_col = head;
        elem->prev_row = NULL;
        head->next_col = elem;
        return elem;
    }

}

void add_elem(SM_elem* head, int row, int col, float val){
    SM_elem* prev_row_elem;
    SM_elem* prev_col_elem;

    prev_row_elem = find_row(head,row);
    prev_col_elem = find_col(head,col);

    if(prev_col_elem == NULL) prev_col_elem = add_col(head,col);
    if(prev_row_elem == NULL) prev_row_elem = add_row(head,row);

    while(prev_col_elem->row < row){
        if(prev_col_elem->next_row != NULL)
            prev_col_elem = prev_col_elem->next_row;
        else break;
    }
    if(prev_col_elem->row>row) prev_col_elem = prev_col_elem->prev_row;

    while(prev_row_elem->col < col){
        if(prev_row_elem->next_col != NULL)
            prev_row_elem = prev_row_elem->next_col;
        else break;
    }
    if(prev_row_elem->col>col) prev_row_elem = prev_row_elem->prev_col;

    SM_elem* elem;
    elem = (SM_elem*)malloc(sizeof(SM_elem));

    elem->col = col;
    elem->row = row;
    elem->val = val;

    elem->prev_col = prev_col_elem;
    elem->next_col = prev_col_elem->next_col;
    prev_col_elem->next_col = elem;

    elem->prev_row = prev_row_elem;
    elem->next_row = prev_row_elem->next_row;
    prev_row_elem->next_row = elem;

    return;
}

void display(SM_elem* head){
    SM_elem* t_row = head;
    SM_elem* t_col = head;

    while(t_row!= NULL){
        t_col = t_row;
        while(t_col != NULL){

            printf("add= %p row= %d col= %d ",t_col,t_col->row, t_col->col);
            printf("val= %f prev_r= %p prev_c= %p ",t_col->val,t_col->prev_row,t_col->prev_col);
            printf("next_r= %p next_c= %p\n",t_col->next_row,t_col->next_col);

            t_col = t_col->next_col;
        }
        t_row = t_row->next_row;
    }

    return;
}

int main()
{
    int rows, cols, num_elems;

    printf("Enter rows, cols, num_elems :: ");
    scanf("%d",&rows);
    scanf("%d",&cols);
    scanf("%d",&num_elems);

    SM_elem* head;
    head = create_head(rows,cols);

    int row, col, val;
    for(int i=0; i<num_elems; i++){
        printf("Enter %d. row col val ::",i);
        scanf("%d",&row);
        scanf("%d",&col);
        scanf("%d",&val);
        add_elem(head,row,col,val);
    }
    display(head);
}
