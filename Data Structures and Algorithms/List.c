#include <stdio.h>
#include <stdlib.h>

#define MAX_ELEMS 20
#define SENTINEL_INDEX 21
#define FORWARD_TRAV 1
#define REVERSE_TRAV 0

#define SORT_INDEX   1
#define SORT_VAL     0

#define TRUE 1
#define FALSE 0

typedef struct list_tag{
    struct elems{
        float val;
        int index;
    }items[MAX_ELEMS];
    int num_items;
    int sorted_index;
    int sorted_val;
} list;

void initialize(list* l){
    for(int i=0; i<MAX_ELEMS; i++)
        l->items[i].index = SENTINEL_INDEX;

    l->num_items = 0;
    l->sorted_index = 0;
    l->sorted_val   = 0;
}

int len(list l){
    return l.num_items;
}

void print(list l, int dir){
    if(dir == FORWARD_TRAV){
        for(int i=0; i<l.num_items; i++)
            printf("%f  ",l.items[i].val);
        printf("\n");
    }
    else if(dir == REVERSE_TRAV){
        for(int i=0; i<l.num_items; i++)
            printf("%f  ", l.items[l.num_items-1-i].val);
    }
    return;
}

void sort(list* l, int to_sort){
    if(to_sort == SORT_INDEX){
        l->sorted_index = TRUE;
        l->sorted_val   = FALSE;
        for(int i=0; i<l->num_items; i++){
            for(int j=i+1; j<l->num_items; j++){
                if(l->items[i].index > l->items[j].index){
                    struct elems temp;
                    temp = l->items[i];
                    l->items[i] = l->items[j];
                    l->items[j] = temp;
                }
            }
        }
    }
    else if(to_sort == SORT_VAL){
        l->sorted_val = TRUE;
        l->sorted_index = FALSE;
        for(int i=0; i<l->num_items; i++){
            for(int j=i+1; j<l->num_items; j++){
                if(l->items[i].val > l->items[j].val){
                    struct elems temp;
                    temp = l->items[i];
                    l->items[i] = l->items[j];
                    l->items[j] = temp;
                }
            }
        }
    }

    return;
}

int find(list* l, int index){
    if(l->sorted_index == FALSE) sort(l,SORT_INDEX);

    int start = 0;
    int end = l->num_items-1;

    while(start<end){
        int mid = (start + end)/2;
        if(l->items[mid].index == index) return mid;
        else if(l->items[mid].index < index) start = mid+1;
        else  end = mid-1;
    }

    return SENTINEL_INDEX;
}

void extract_val(list* l, int index){
    int i = find(l, index);

    if(i == SENTINEL_INDEX) printf("Element doesn't exist.\n");
    else
        printf("The value stored in that index = %f\n", l->items[i].val);

    return;
}

int insert(list* l,float v, int i){
    if(find(l,i) != SENTINEL_INDEX){
        printf("The index already contains data.\n");
        return FALSE;
    }
    else if(l->num_items < MAX_ELEMS){
        l->items[l->num_items].index = i;
        l->items[l->num_items++].val   = v;

        l->sorted_index = FALSE;
        l->sorted_val   = FALSE;

        return TRUE;
    }
    else return FALSE;
}

int overwrite(list* l,float val, int index){
    int i;
    if((i=find(l,index)) != SENTINEL_INDEX){
        l->items[i].val = val;

        l->sorted_index = FALSE;
        l->sorted_val   = FALSE;

        return TRUE;
    }
    return FALSE;
}

int delete(list* l, int index){
    int i;
    if((i=find(l,index)) != SENTINEL_INDEX){
        l->items[i] = l->items[l->num_items-1];
        l->items[l->num_items].index = SENTINEL_INDEX;
        l->num_items--;

        return TRUE;
    }
    else return FALSE;
}
