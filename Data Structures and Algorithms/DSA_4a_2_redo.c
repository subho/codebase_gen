#include <stdio.h>
#include <stdint.h>
#include <stdlib.h>
#include <time.h>

#define TRUE 1
#define FALSE 0
#define NUM_Q 5000

typedef struct quee{
    uint16_t row;
    uint16_t col;
    uint32_t violation;
    uint32_t mutation;
}queen;


uint8_t isViolating(uint16_t x, uint16_t y, uint16_t h, uint16_t k){
    if(x == h || y == k || (x+y)==(h+k) || (x-h)==(y-k))
        return TRUE;
    else return FALSE;
}

void initilialize(queen queens[NUM_Q]){
    srand(time(0));
    for(int i=0; i<NUM_Q; i++){
        queens[i].col = i;
        queens[i].row = rand()%NUM_Q;
        queens[i].violation = 0;
        queens[i].mutation = 0;
    }

    for(int i=0; i<NUM_Q; i++){
        for(int j=i+1; j<NUM_Q; j++){
            if(isViolating(queens[i].row, queens[i].col, queens[j].row, queens[j].col)){
                queens[i].violation++;
                queens[j].violation++;
            }
        }
    }
}

short int compare(queen a, queen b){
    if(a.violation < b.violation)
        return -1;
    else if(a.violation > b.violation)
        return 1;
    else{
        if(a.mutation < b.mutation)
            return 1;
        else if(a.mutation > b.mutation)
            return -1;
        else return 0;
    }
}

void swap(queen* a, queen* b){
    queen tmp = *a;
    *a = *b;
    *b = tmp;
}

void q_sort(queen queens[NUM_Q],int start, int end){
    if(start < end){
        uint16_t part_ind = start;
        uint16_t arr_start = start+1;
        uint16_t arr_end = end;

        while(arr_start < arr_end){
            while(compare(queens[arr_start], queens[part_ind]) == 1)
                arr_start++;
            while(compare(queens[arr_end], queens[part_ind]) == -1)
                arr_end--;
            while(compare(queens[arr_start], queens[part_ind]) == 0){
                if(rand()%2) swap(queens+arr_start, queens+part_ind);
                arr_start++;
            }
            if(arr_start<arr_end)
                swap(queens+arr_end, queens+arr_start);
        }
        swap(queens+arr_end, queens+part_ind);
        q_sort(queens,start, arr_end-1);
        q_sort(queens,arr_start, end);
    }
}

void update(queen queens[NUM_Q], uint16_t col, uint16_t prev_row, uint16_t next_row){
    uint8_t currViolate[NUM_Q] = {0};
    uint16_t nextViolate[NUM_Q] = {0};

    for(int i=0; i<NUM_Q; i++){
        if(queens[i].col == col){
            currViolate[i] = 2;
            nextViolate[i] = 2;
        }
        else{
            if(isViolating(prev_row, col,queens[i].row, queens[i].col))
                currViolate[i] = 1;
            else currViolate[i] = 0;
            if(isViolating(next_row, col,queens[i].row, queens[i].col))
                nextViolate[i] = 1;
            else nextViolate[i] = 0;
        }
        queens[i].violation += nextViolate[i]-currViolate[i];
    }
}

void print(uint32_t stage, queen queens[NUM_Q]){
    printf("---------------------------------------------------------------\n");
    printf("Stage = %d\n", stage);
    for(int i=0; i<NUM_Q; i++){
        printf("|row = %d, col = %d|\t",queens[i].row, queens[i].col);
        printf("|violation = %d, mutation = %d|\n",queens[i].violation, queens[i].mutation);
    }
    printf("---------------------------------------------------------------\n");
}

uint32_t solve(queen queens[NUM_Q], uint32_t t_stages){
    uint32_t stage = 1;
    q_sort(queens,0,NUM_Q-1);
    while(queens[0].violation != 0 && stage<=t_stages){
        uint16_t numViolations;
        uint16_t index = NUM_Q+1;
        uint16_t b_index = NUM_Q-1;
        for(int i=0; i<NUM_Q; i++){
            numViolations = 0;
            for(int j=1; j<NUM_Q; j++)
                if(isViolating(i, queens[0].col, queens[j].row, queens[j].col))
                    numViolations++;
            if(numViolations < queens[0].violation){
                index = i;
                break;
            } else if(numViolations ==  queens[0].violation && i!=queens[0].row){
                if(rand()%5 == 4) b_index = i;
            }
        }

        if(index != NUM_Q+1){
            update(queens, queens[0].col, queens[0].row, index);
            queens[0].row = index;
            queens[0].violation = numViolations;
            queens[0].mutation++;
        } else{
            update(queens, queens[0].col, queens[0].row, b_index);
            queens[0].row = b_index;
            queens[0].mutation++;
        }

        q_sort(queens, 0, NUM_Q-1);
        //print(stage, queens);
        printf("Stage = %d\n",stage);
        stage++;
    }
    return stage;
}



int main(){
    queen queens[NUM_Q];
    initilialize(queens);
    print(0,queens);
    uint32_t stage = solve(queens, 10000);
    print(stage,queens);
}
