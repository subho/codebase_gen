#include <stdio.h>
#include <stdlib.h>

#define MEM_SIZE 1000
#define S_INDEX  -1
#define TRUE  1
#define FALSE 0

typedef struct{
    float val;
    int   next;
}node;

typedef struct{
    node MEM_STORE[MEM_SIZE];
    int  free_index;
}MEMORY;

MEMORY MEM;

void initialize_mem(){
    for(int i=0;i<MEM_SIZE; i++){
        MEM.MEM_STORE[i].val  = 0;
        MEM.MEM_STORE[i].next = i+1;
    }
    MEM.MEM_STORE[MEM_SIZE-1].next = S_INDEX;
    MEM.free_index = 0;
}

int alloc_mem(){
    if(MEM.free_index == S_INDEX) return S_INDEX;
    int prev_free = MEM.free_index;
    MEM.free_index = MEM.MEM_STORE[prev_free].next;
    return prev_free;
}

void dealloc_mem(int index){
    MEM.MEM_STORE[index].next = MEM.free_index;
    MEM.free_index = index;
}

void init_l(int *head){
    *head = S_INDEX;
    return;
}

int empty_l(int head){
    if(head == S_INDEX) return TRUE;
    else return FALSE;
}

int atend_l(int tail){
    if(MEM.MEM_STORE[tail].next == S_INDEX)
        return TRUE;
    else return FALSE;
}

void insert_front(int target, int* head){
    MEM.MEM_STORE[target].next = *head;
    *head = target;
}

void insert_after(int target, int prev){
    MEM.MEM_STORE[target].next = MEM.MEM_STORE[prev].next;
    MEM.MEM_STORE[prev].next   = target;
}

void delete_front(int* head){
    int target = *head;
    *head = MEM.MEM_STORE[*head].next;
    dealloc_mem(target);
}

void delete_after(int prev){
    int target = MEM.MEM_STORE[prev].next;
    MEM.MEM_STORE[prev].next = MEM.MEM_STORE[target].next;
    dealloc_mem(target);
}

int create_elem(float val){
    int index = alloc_mem();
    if(index == S_INDEX) printf("No space to allocate!\n");
    else{
        MEM.MEM_STORE[index].val = val;
        MEM.MEM_STORE[index].next = S_INDEX;
    }
    return index;
}

void print_forward(int head){
    while(head != S_INDEX){
        if(!atend_l(head))
            printf("%.2f<--->",MEM.MEM_STORE[head].val);
        else printf("%.2f",MEM.MEM_STORE[head].val);
        head = MEM.MEM_STORE[head].next;
    }
    printf("\n");
}

void print_reverse(int head){
    if(head != S_INDEX) print_reverse(MEM.MEM_STORE[head].next);
    printf("%f<--->", MEM.MEM_STORE[head].val);
    return;
}

int size_of(int head){
    int count = 0;
    while(head != S_INDEX){
        count++;
        head = MEM.MEM_STORE[head].next;
    }
    return count;
}

int is_equal(int head1, int head2){
    if(size_of(head1)==size_of(head2)){
        while(head1 != S_INDEX){
            if(MEM.MEM_STORE[head1].val == MEM.MEM_STORE[head2].val){
                head1 = MEM.MEM_STORE[head1].next;
                head2 = MEM.MEM_STORE[head2].next;
            }
            else return FALSE;
        }
        return TRUE;
    }
    else return FALSE;
}

void append(int head1, int head2){
    while(!atend_l(head1))
        head1 = MEM.MEM_STORE[head1].next;

    MEM.MEM_STORE[head1].next = head1;
    return;
}

int is_ordered(int head){
    int is_increasing = 5;
    int is_equal   = 0;
    int is_decreasing  = 2;
    int is_what    = is_equal;
    int next_index = S_INDEX;

    while(!atend_l(head)){
        next_index = MEM.MEM_STORE[head].next;
        if(MEM.MEM_STORE[head].val == MEM.MEM_STORE[next_index].val)
            is_what = is_what|is_equal;
        else if(MEM.MEM_STORE[head].val > MEM.MEM_STORE[next_index].val)
            is_what = is_what|is_decreasing;
        else is_what = is_what|is_increasing;

        head = next_index;
    }

    if(is_what == (is_increasing|is_decreasing))
        return FALSE;
    else return TRUE;
}

void swap(int index1, int index2){
    float temp = MEM.MEM_STORE[index1].val;
    MEM.MEM_STORE[index1].val = MEM.MEM_STORE[index2].val;
    MEM.MEM_STORE[index2].val = temp;
}

void sort(int head){
    int loop1 = head;
    int loop2 = S_INDEX;

    while(!atend_l(loop1)){
        loop2 = MEM.MEM_STORE[loop1].next;
        while(!atend_l(loop2)){
            if(MEM.MEM_STORE[loop1].val > MEM.MEM_STORE[loop2].val)
                swap(loop1,loop2);
            loop2 = MEM.MEM_STORE[loop2].next;
        }
        loop1 = MEM.MEM_STORE[loop1].next;
    }
}

int merge_l(int head1, int head2){
    int head = S_INDEX;
    int curr = head;
    float val1 = 0;
    float val2 = 0;
    while((head1 != S_INDEX)||(head2 != S_INDEX)){
        if(head1 == S_INDEX){
            insert_after(create_elem(MEM.MEM_STORE[head2].val),curr);
            head2 = MEM.MEM_STORE[head2].next;
            curr  = MEM.MEM_STORE[curr].next;
        }
        else if(head2 == S_INDEX){
            insert_after(create_elem(MEM.MEM_STORE[head1].val),curr);
            head1 = MEM.MEM_STORE[head1].next;
            curr  = MEM.MEM_STORE[curr].next;
        }
        else{
            val1 = MEM.MEM_STORE[head1].val;
            val2 = MEM.MEM_STORE[head2].val;

            if(val1<val2){
                if(head == S_INDEX){
                    insert_front(create_elem(val1),&head);
                    curr = head;
                }
                else{
                    insert_after(create_elem(val1),curr);
                    curr = MEM.MEM_STORE[curr].next;
                }
                head1 = MEM.MEM_STORE[head1].next;
            }
            else{
                if(head == S_INDEX){
                    insert_front(create_elem(val2),&head);
                    curr = head;
                }
                else{
                    insert_after(create_elem(val2),curr);
                    curr = MEM.MEM_STORE[curr].next;
                }
                head2 = MEM.MEM_STORE[head2].next;
            }
        }
    }
    return head;
}

void del_list(int *head){
    while(*head!=S_INDEX){
        delete_front(head);
    }
}

void del_alt_list(int head){
    int i=1;
    while(head != S_INDEX){
        if(i%2 == 1)
            delete_after(head);
        else head = MEM.MEM_STORE[head].next;

        i=i+1;
    }
    return;
}

void del_duplicates(int head, int is_sorted){
    if(is_sorted == FALSE) sort(head);
    int next_elem;
    while(!atend_l(head)){
        next_elem = MEM.MEM_STORE[head].next;
        if(MEM.MEM_STORE[head].val == MEM.MEM_STORE[next_elem].val)
            delete_after(head);
        else head = next_elem;
    }
}

void rotate_list(int head){
    int temp_val = MEM.MEM_STORE[head].val;
    int curr = head;
    int next_elem = S_INDEX;
    while(!atend_l(curr)){
        next_elem = MEM.MEM_STORE[curr].next;
        MEM.MEM_STORE[curr].val = MEM.MEM_STORE[next_elem].val;
        curr = next_elem;
    }
    MEM.MEM_STORE[curr].val = temp_val;
}

void read_from_file(FILE* file, int* head){
    float val = 0;
    int curr = *head;
    while(fread(&val,sizeof(float),1,file)){
        if(*head == S_INDEX){
            insert_front(create_elem(val),head);
            curr = *head;
        }
        else{
            insert_after(create_elem(val),curr);
            curr = MEM.MEM_STORE[curr].next;
        }
    }
    return;
}

int main(){
    FILE* file1 = fopen("NUM_LIST","r");
    FILE* file2 = fopen("NUM_LIST2","r");

    initialize_mem();

    if(file1 == NULL /*|| file2 == NULL*/){
        perror("Sorry. Couldn't open the files.\n");
        exit(1);
    }

    int head1 = S_INDEX;
    int head2 = S_INDEX;

    read_from_file(file1,&head1);
    print_forward(head1);


}
