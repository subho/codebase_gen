#include <stdio.h>
#include <stdlib.h>

void merge(int* arr, int start, int mid, int end){
    int *temp = (int* )malloc(sizeof(int)*(end-start+1));

    int arr_1_start = start;
    int arr_2_start = mid+1;
    for(int i=0; i<=end-start; i++){
        if(arr_1_start>mid)
            temp[i] = arr[arr_2_start++];
        else if(arr_2_start>end)
            temp[i] = arr[arr_1_start++];
        else if(arr[arr_1_start]<arr[arr_2_start])
            temp[i] = arr[arr_1_start++];
        else temp[i] = arr[arr_2_start++];
    }

    for(int i=start; i<=end; i++)
        arr[i] = temp[i-start];

    free(temp);
}

void mergesort(int* arr, int start, int end){
    if(start<end){
        int mid = (start+end)/2;
        mergesort(arr, start, mid);
        mergesort(arr, mid+1, end);
        merge(arr, start, mid, end);
    }
}
