#include <stdio.h>
#include <stdlib.h>

void swap(int* a, int* b){
    int temp = *a;
    *a = *b;
    *b = temp;
}

void adjust(int* arr, int root, int end){
    int temp = arr[root];
    int child = 2*root + 1;

    while(child <= end){
        if(child < end && arr[child]<arr[child+1])
            child++;
        if(temp<arr[child]){
            arr[root] = arr[child];
            root = child;
            child = 2*root + 1;
        }
        else break;
    }
    arr[root] = temp;
}

void heapsort(int* arr, int start, int end){
    for(int i=(end-1)/2; i>=0; i--)
        adjust(arr,i,end);

    for(int i=0; i<end; i++){
        swap(&arr[0],&arr[end-i]);
        adjust(arr,0,end-i-1);
    }
}
