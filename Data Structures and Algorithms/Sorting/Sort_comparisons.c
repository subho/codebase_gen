#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include "insertion_sort.h"
#include "merge_sort_iter.h"
#include "quicksort.h"
#include "heapsort.h"
#include <time.h>

#define DEBUG 0
#define TRUE  1
#define FALSE 0

struct exec_stats{
    int size;
    double randomness;
    unsigned long long int insert_exec_time;
    unsigned long long int merge_exec_time;
    unsigned long long int quick_exec_time;
    unsigned long long int heap_exec_time;
};

int* create_arr(int size){
    int* arr = (int *)malloc(sizeof(int)*size);
    if(arr == NULL){
        perror("MEM COULDN'T BE ALLOCATED \n");
        return NULL;
    }
    else{
        srand((int)arr);
        for(int i=0; i<size; i++)
            arr[i] = rand()%size;
        return arr;
    }
}

double rand_calc(int* arr, int size){
    if(size<=1) return 0;

    int is_increasing = 2;
    int is_decreasing = 5;
    int is_equal      = 0;
    int seq_flips     = 0;
    int is_in_sequence = 0;

    for(int i=1;i<size;i++){
        int prev_seq = is_in_sequence;
        if(arr[i-1] == arr[i]) is_in_sequence |= is_equal;
        else if(arr[i-1] >  arr[i]) is_in_sequence |= is_decreasing;
        else if(arr[i-1] <  arr[i]) is_in_sequence |= is_increasing;

        if(is_in_sequence == 7){
            seq_flips++;
            is_in_sequence = is_in_sequence^prev_seq;
        }
    }

    double randomness = (double)seq_flips/(size-1);
    return randomness;
}

void mutate(int* arr, int size){
    srand((int)arr);
    for(int i=0; i<size*2; i++)
        swap(arr+rand()%size, arr+rand()%size);
}

void print_arr(int* arr, int size){
    for(int i=0; i<size; i++)
        printf("%5d ",arr[i]);

    printf("\n");
}

int main(){
    int size = 5;
    FILE* file = fopen("sort_stats.txt","w");
    clock_t clock_start;
    clock_t clock_end;

    fprintf(file,"Size,Randomness,Insertion_sort,Mergesort,Quicksort,Heapsort\n");
    int exclude_insertion_sort = FALSE;
    struct exec_stats stat;
    while((size++)<100000){
        int* init_arr = create_arr(size);
        int* cpy_arr = (int *)malloc(sizeof(int)*size);
        stat.size = size;
        for(int i=0; i<2*size; i++){

            stat.randomness = rand_calc(init_arr,size);

            if(!exclude_insertion_sort){
                memcpy(cpy_arr,init_arr, sizeof(int)*size);
                clock_start = clock();
                insertion_sort(cpy_arr,size);
                clock_end   = clock();
                stat.insert_exec_time = (double)(clock_end-clock_start);
                if(DEBUG) printf("%lf\n",rand_calc(cpy_arr,size) );
                 if(stat.insert_exec_time > 120000) exclude_insertion_sort = TRUE;
            }
            else stat.insert_exec_time = 0;           

            memcpy(cpy_arr,init_arr, sizeof(int)*size);
            clock_start = clock();
            mergesort(cpy_arr,0,size-1);
            clock_end   = clock();
            stat.merge_exec_time = (clock_end-clock_start);
            if(DEBUG) printf("%lf\n",rand_calc(cpy_arr,size) );

            memcpy(cpy_arr,init_arr, sizeof(int)*size);
            clock_start = clock();
            quicksort(cpy_arr,0,size-1);
            clock_end   = clock();
            stat.quick_exec_time = (clock_end-clock_start);
            if(DEBUG) printf("%lf\n",rand_calc(cpy_arr,size) );

            memcpy(cpy_arr,init_arr, sizeof(int)*size);
            clock_start = clock();
            heapsort(cpy_arr,0,size-1);
            clock_end   = clock();
            stat.heap_exec_time = (clock_end-clock_start);
            if(DEBUG) printf("%lf\n",rand_calc(cpy_arr,size) );

            fprintf(file,"%d,%lf,%lld,%lld,%lld,%lld\n",stat.size,stat.randomness,stat.insert_exec_time,stat.merge_exec_time,stat.quick_exec_time,stat.heap_exec_time);
            mutate(init_arr, size);

            if(i == size){
                memcpy(init_arr,cpy_arr,sizeof(int)*size);
            }
        }
        free(cpy_arr);
        free(init_arr);
    }
    fclose(file);

    return 0;
}
