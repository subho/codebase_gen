#include <stdio.h>
#include <stdlib.h>

void insertion_sort(int* arr, int size){
    for(int i=0; i<size; i++){
        int key = arr[i];
        for(int j=i-1; j>=0; j--){
            if(arr[j]>key){
                arr[j+1] = arr[j];
                arr[j]   = key;
            }
        }
    }
}
