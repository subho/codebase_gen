#include <stdio.h>
#include <stdlib.h>

#define MAX_ELEMS 20
#define TRUE      1
#define FALSE     0

int* create_heap(int size){
    int* heap = (int *)malloc(sizeof(int)*size);

    for(int i=0; i<size; i++)
        heap[i] = rand()%100;

    return heap;
}

int* max(int* a, int* b){
    if(*a>*b) return a;
    else return b;
}

void swap(int* a, int* b){
    int temp = *a;
    *a = *b;
    *b = temp;

    return;
}

void print_heap(int* heap, int size){
    for(int i=0; i<size; i++)
        printf("%5d  ",heap[i]);
    printf("\n");
}

void max_heapify(int* heap,int index, int size){
    int left = 2*index+1;
    int right = 2*(index+1);
    int largest = index;

    if(left<size && heap[left]>heap[largest])
        largest = left;

    if(right<size && heap[right]>heap[largest])
        largest = right;

    if(largest != index){
        swap(&heap[largest], &heap[index]);
        max_heapify(heap,largest,size);
    }

    print_heap(heap,size);
    return;
}

void heapify(int* heap, int size){
    for(int i=size/2-1;i>=0;i--)
        max_heapify(heap,i,size);
}

void heapsort(int* heap, int size){
    for(int i=0; i<size-1; i++){
        swap(&heap[0],&heap[size-1-i]);
        max_heapify(heap,0,size-i-1);
    }
}

int main(){
    int* heap = create_heap(MAX_ELEMS);

    for(int i=0; i<MAX_ELEMS; i++)
        printf("%5d  ",i+1);
    printf("\n");
    print_heap(heap, MAX_ELEMS);
    heapify(heap,MAX_ELEMS);
    heapsort(heap, MAX_ELEMS);
    print_heap(heap,MAX_ELEMS);

    return 0;
}
