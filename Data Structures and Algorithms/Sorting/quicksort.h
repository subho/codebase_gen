#include <stdio.h>
#include <stdlib.h>

void q_swap(int* a, int* b){
    int temp = *a;
    *a = *b;
    *b = temp;
}

int* median_3(int* start, int* mid, int* end){
    if((*start - *mid)*(*mid - *end) >=0) return mid;
    else return median_3(mid,end,start);
}

void quicksort(int* arr, int start, int end){
    if(start<end){
        int mid = (start+end)/2;
        int part_ind = median_3(arr+start,arr+mid,arr+end) - arr;

        q_swap(arr+start, arr+part_ind);
        part_ind = start;

        int arr_end = end;
        int arr_start = start;

        while(arr_start<arr_end){
            while(arr[arr_start] <= arr[part_ind]) arr_start++;
            while(arr[arr_end] > arr[part_ind]) arr_end--;
            if(arr_start<arr_end) q_swap(&arr[arr_start], &arr[arr_end]);
        }

        q_swap(&arr[arr_end],&arr[part_ind]);

        quicksort(arr, start, arr_end-1);
        quicksort(arr, arr_end+1, end);
    }
}
