#include <stdio.h>
#include <stdlib.h>

#define TRUE 1
#define FALSE 0

typedef struct node_tag{
    int index;
    short int visited;
    float weight;
    struct node_tag* next_v;
}vertex;

void initList(vertex** arrNode, int size){
    for(int i=0; i<size; i++)
        arrNode[i] = NULL;
}

vertex* createVertex(int ind, float weight){
    vertex* temp = malloc(sizeof(vertex*));
    temp->index = ind;
    temp->visited = FALSE;
    temp->weight = weight;
    temp->next_v = NULL;
    return temp;
}

void InsertEdge(vertex** arrNode, int u, vertex* v){
    if(arrNode[u] == NULL)
        arrNode[u] = v;
    else{
        vertex* i = NULL;
        for(i = arrNode[u]; i->next_v!=NULL;)
             i=i->next_v;
        i->next_v = v;
    }
}

void deleteEdge(vertex** arrNode, int u, int v){
    vertex* prev = NULL;
    for(vertex* i = arrNode[u]; i!=NULL;i=i->next_v){
        if(i->index == v){
            if(prev == NULL){
                prev = i;
                arrNode[u] = i->next_v;
                free(prev);
            } else {
                prev->next_v = i->next_v;
                free(i);
            }
            break;
        }else
            prev = i;
    }
}

void printGraph(vertex** arrNode, int size){
    for(int i=0; i<size; i++){
        if(arrNode[i] != NULL){
            printf("%d = { ",i+1);
            vertex* temp = arrNode[i];
            while(temp){
                printf(" <%d, %f> ", temp->index+1, temp->weight);
                temp = temp->next_v;
            }
            printf(" }\n");
        }
    }
}

void BFS(vertex** arrNode, int s, int size){

}

int main(){
    printf("This is a program to test algoritms for Directed Graphs\n\n");
    int n;
    printf("Enter the number of vertices :: ");
    scanf("%d",&n);

    vertex** arrNode = (vertex **)malloc(sizeof(vertex*)*n);
    initList(arrNode,n);

    int u, v;
    float w;

    int cntnu = FALSE;
    do{
        printf("Enter the starting node <--> ending node <--> weight :: ");
        scanf("%d", &u);
        scanf("%d", &v);
        scanf("%f", &w);
        InsertEdge(arrNode, u-1, createVertex(v-1, w));
        printf("Do you want to insert more Links :: ");
        scanf("%d",&cntnu);
    }while(cntnu);

    printf("Illustrating the graph as represented ::: \n");
    printGraph(arrNode, n);

    return 0;
}
