#include <stdio.h>
#include <stdlib.h>

#define TRUE  1
#define FALSE 0

struct link_tag;

typedef struct vertex_tag{
    int is_used;
    float val;
    struct vertex_tag* next_v;
    struct link_tag* links;
} vertex;

typedef struct link_tag{
    vertex* vrtx;
    struct link_tag* next_l;
}link;

vertex* create(){
    return NULL;
}

void InsertVertex(vertex** graph, vertex* v){
    vertex* temp = *graph;
    if(*graph == NULL) *graph = v;
    else{
        while((temp)->next_v != NULL)
            temp = temp->next_v;
        temp->next_v = v;
    }

    return;
}

link* create_links(vertex* v){
    link* temp = (link *)malloc(sizeof(link));
    temp->vrtx = v;
    temp->next_l = NULL;
    return temp;
}

void InsertEdge(vertex* v1, vertex* v2){
    if(v1->links == NULL)
        v1->links = create_links(v2);
    else{
        link* temp;
        temp = v1->links;
        v1->links = create_links(v2);
        v1->links->next_l = temp;
    }
}

void DeleteVertex(vertex** graph, vertex* prev_v){
    vertex* temp = prev_v->next_v;
    prev_v->next_v = temp->next_v;
    free(temp);
}

void DeleteEdge(vertex* v1, vertex* v2){
    if(v1->links->vrtx == v2){
        free(v1->links);
        return;
    }

    for(link* prev_l = v1->links;prev_l->next_l != NULL;prev_l=prev_l->next_l){
        if(prev_l->next_l->vrtx == v2){
            link* temp = prev_l->next_l;
            free(prev_l->next_l);
            prev_l->next_l = temp->next_l;
            return;
        }
    }
}
