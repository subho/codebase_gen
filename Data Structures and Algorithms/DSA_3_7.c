/**********************************************************************
* Design objective: To create a memory manager for heap
*                   simulation.
* Design goals ::
* 1. Able to borrow a block of memory from available space
* 2. Able to allocate this memory for different user needs
* 3. Able to free the memory as and when user needs it
* 4. Able to add more blocks of memory is necessary.
* 5. Able to free a block of memory
*       a. Physical deletion of block by returning the memory
*          back to the system
*       b. Preserving the block for future reuse | Logical
*          deletion.
* 6. Maintain a garbage collector [However don't think about
*                                  right now]
*
* Sources of knowledge :: In my pocket
*
* Data Structures to be used :
* 1. Doubly linked List Data Structure at block level
* 2. Heap Data Structure for maintaing free space ::
*       a. Implemented on top of Doubly Linked list (XORed)
*       b. Size of the free space will act as key
*       c. The heap will follow min-heapified
*
* Structure of a block ::
* 1. General details of the field
*       a. The address of next block         :: pointer field (8 bytes)
*       b. Maximum size of free space        :: integer field (2 bytes)
*       c. The root address of free memory   :: pointer field (8 bytes)
* 2. The space that will be given to user
*       a. The free space will also be a loose struct ::
*               i. By loose, I mean, there will be no struct for
*                  defining the free space. However the general
*                  fields will be present, in order, as stated below.
*              ii. Size of the free space    :: integer field (2 bytes)
*             iii. XOR(prev_addr, next_addr) :: integer field (2 bytes)
*                  prev_addr for root -> 0
*                  next_addr for tail -> 0
*       b. The allocated space will have the following header ::
*               i. Size (payload + itself) :: integer field (2+payload
*                                                               bytes)
*              ii. Distance from beginning   :: integer field (2 bytes)
*                  of user space
**********************************************************************/
#include <stdio.h>
#include <stdlib.h>

#define DEF_USR_SPACE 200
#define TRUE          1
#define FALSE         0
#define MAX_FREE_BLOCKS 20
#define SENTINEL_INDEX  -1

/**********************************************************************
* (+) All types of block lists will be circularly linked without
*     inclusion of their respective block heads.
* (+) The list will be ordered chronologically, i.e., point of
*     introduction in the list
* (+) There will be 2 lists of blocks ::
*       1. The block tail will point to the last block inserted
*       2. Number of blocks present in the list
***********************************************************************/

typedef struct block_tag{
    struct block_tag* next;
    short int         initialSize;
    short int         maxFreeSize;
    void*             FreeSpaceRoot;
}block;

typedef struct block_info_tag{
    block* tail;
    short int num_blocks;
} block_info;

block_info BlocksInUse;
block_info BlocksFree;

/**********************************************************************
The in memory representation of Free blocks
------------------
|   size     | a |
------------------
|   prev         |
------------------
|   next         |
------------------
|++++++++        |
|++++++++        |
------------------
|   size     | a |
------------------

The in memory representation of allocated blocks
------------------
|   size     | a |
------------------
|++++++++        |
|++++++++        |
------------------
|   size     | a |
------------------
***********************************************************************/

void init_usr_space(block *b){
    /* block point is used to point to the block from where we need to
       start writing*/
    char* block_point = (char *)b;
    short int* block_write = NULL;
    /*write down the size of the block*/
    /*Right shift the value stored and use the last bit as free tag*/
    block_point = b->FreeSpaceRoot;
    block_write = (short *)block_point;
    *block_write = (b->initialSize)<<1;

    /*Set the previous pointer as sentinel index*/
    block_write += 1;
    *block_write = SENTINEL_INDEX;

    /*Set the next pointer as sentinel index    */
    block_write += 1;
    *block_write = SENTINEL_INDEX;

    /**/

}

block* create_block(int size){
    if(size < DEF_USR_SPACE) size = DEF_USR_SPACE;

    block* new_block = (block *)malloc(sizeof(block) + size);
    new_block->next  = NULL;
    new_block->initialSize = size;
    new_block->maxFreeSize = size;
    new_block->FreeSpaceRoot = (void *)((char *)new_block + sizeof(block));
    init_usr_space(new_block);

    return new_block;
}

/***********************************************************************
* Now, we'll define 2 basic opertions ::
* 1. Addtion of block to a block list
* 2. Physical deletion of a block from a list.
* These 2 basic operation will be used by other functions which will
* decide on appropiate utilization of these 2 functions
**********************************************************************/

/***********************************************************************
* add_block(block_info* info_block ,block* target) ::
* This function can be used for the following puposes ::
*   1. It can be used to transfer a block from Used_block_list to
*      Free_block_list
*   2. It can be used to add newly created blocks.
***********************************************************************/
void add_block(block_info* info_block, block* target){
    info_block->num_blocks++;
    if(info_block->tail == NULL){
        info_block->tail = target;
        target->next     = target;
    }
    else{
        target->next = info_block->tail->next;
        info_block->tail->next = target;
        info_block->tail       = target;
    }
}

/*********************************************************************
* void del_block(block_head *info_block, block* prev) ::
* This function physically deletes a block. The possible uses are :
*   1. If the number of free blocks is more than a certain thresold,
*      we can return a few blocks to prevent exhaustion of memory
*********************************************************************/
void del_block(block_info *info_block, block* prev){
    info_block->num_blocks --;
    block* temp = prev->next;
    prev->next = temp->next;

    if(info_block->tail == temp)
        info_block->tail = prev;
    free(temp);
}

/*********************************************************************
* block* rem_block(block_info *info_block, block* prev) ::
* This function removes the block after prev from the list and returns
* the address of the removed block. The posible use cases are:
* 1. To remove a block from the list and append it to another lists
* 2. To be used in conjunction with add block often
*********************************************************************/
block* rem_block(block_info* info_block, block* prev){
    info_block->num_blocks--;
    block* temp = prev->next;
    prev->next = temp->next;

    if(info_block->tail == temp)
        info_block->tail = prev;

    return temp;
}

/*********************************************************************
* block* fetch_block(int size) ::
* Find a block in your list which conforms to the size specified. For
* this, the steps are ::
* 1. Check if the current blocks in blocks_in_Use can handle the
*    requested size. If so return one such block.
* 2. Check if the current block in blocks_Free can handle the
*    requested size. If so, return the block with smallest size. Then,
*    append the block to blocks_in_use. Return the address of the
*    block.
* 3. If nothing succeeds, go for creation of one such block and
*    append it to the list.
*********************************************************************/

block* fetch_block(int size){
    if(BlocksInUse.num_blocks != 0){
        block* iterator = BlocksInUse.tail->next;
        do{
            if(iterator->maxFreeSize >= size)
                return iterator;
            iterator = iterator->next;
        }while(iterator != BlocksInUse.tail->next);
    }

    block* block_to_use = NULL;

    if(BlocksFree.num_blocks != 0){
        block* iterator = BlocksFree.tail;
        int min_size = -1;
        block* prev = iterator;
        do{
            if(iterator->maxFreeSize >= size){
                if(min_size == -1){
                    min_size = iterator->maxFreeSize;
                    block_to_use = prev;
                }
                else if(iterator->maxFreeSize < min_size){
                    min_size = iterator->maxFreeSize;
                    block_to_use = prev;
                }
            }
            prev = iterator;
            iterator = iterator->next;
        }while(iterator != BlocksFree.tail);
    }

    if(block_to_use == NULL) block_to_use = create_block(size);
    else block_to_use = rem_block(&BlocksFree, block_to_use);

    add_block(&BlocksInUse, block_to_use);
    return block_to_use;
}

void mem_alloc(int size){

}

int main(){
    fetch_block(10);
}
