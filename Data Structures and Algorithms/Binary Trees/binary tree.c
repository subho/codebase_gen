#include <stdio.h>
#include <stdlib.h>
#include <stdint.h>

#define TRUE 1
#define FALSE 0

typedef struct nodeTag{
    int data;
    struct nodeTag* leftChild;
    struct nodeTag* rightChild;
}node;

node* Create(){
    return NULL;
}

uint8_t IsEmpty(node* bt){
    if(bt == NULL)
        return TRUE;
    else return FALSE;
}

node* MakeBT(node* lt, int data, node* rt){
    node* temp = (node*) malloc(sizeof(node));
    temp-> leftChild = lt;
    temp-> rightChild = rt;
    temp-> data = data;
    return temp;
}

node* Lchild(node* bt){
    if(IsEmpty(bt->leftChild)){
        perror("No left child.\n");
        return NULL;
    } else return bt->leftChild;
}

int Data(node* bt){
    if(IsEmpty(bt)){
        perror("No data present.\n");
        exit(1);
    } else return bt->data;
}

node* Rchild(node* bt){
    if(IsEmpty(bt->rightChild)){
        perror("No right child. \n");
        return NULL;
    } else return bt->rightChild;
}

void inorder(node* ptr){
    if(!IsEmpty(ptr->leftChild))
        inorder(ptr->leftChild);
    printf("%d\n", );
}
