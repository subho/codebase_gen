#include <stdio.h>
#include <stdlib.h>

#define TRUE 1
#define FALSE 0

typedef struct {
    int key;
}element;

typedef struct treeNodeTag{
    struct treeNodeTag* leftChild;
    element data;
    short int bf;
    struct treeNodeTag* rightChild;
}treeNode;

treeNode* create(){
    return NULL;
}

/********************************************
* Right rotate(z)
*           z                        y
*         /   \                   /    \
*        y    T4                x       z
*      /  \                   /  \    /  \
*     x    T3     <----->    T1  T2  T3  T4
*   /  \
*  T1  T2
**********************************************/
void leftLeftBalance(treeNode** pivot){
    treeNode* z = (*pivot);
    treeNode* y  = z->leftChild;
    treeNode* T3 = y->rightChild;

    (*pivot) = y;
    y->rightChild = z;
    z->leftChild = T3; 
}
