#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <stdint.h>

#define true  1
#define false 0
#define MAXNUMSIZE 10
#define DEFeXPRsIZE 20

typedef enum{exp = 7, pOpen=6, pClose=5, dvsn=4, mul=3, add=2, sub=1, val=0} operator;

typedef struct nodeTag{
    struct nodeTag* prev;
    operator op;
    float dat;
    struct nodeTag* next;
}node;

float strToFloat(char* strng){
    float num = 0;
    uint8_t foundDot = false;
    int pow10 = 10;

    int strnglen = strlen(strng);

    for(int i=0;i<strnglen; i++){
        if(strng[i] == '.')
            foundDot = true;
        else{
            if(!foundDot)
                num = num*10 + strng[i]-'0';
            else{
                num = num + (strng[i] - '0')/pow10;
                pow10 *= 10;
            }
        }
    }

    return num;
}

uint8_t isIntOrDot(char c){
    if(c>= '0' && c<= '9')
        return true;
    else if(c == '.')
        return true;
    else return false;
}

operator identifyOp(char c){
    switch(c){
        case '(': return pOpen;
        case ')': return pClose;
        case '/': return dvsn;
        case '*': return mul;
        case '-': return sub;
        case '+': return add;
        case '^': return exp;
        default: return val;
    }
}

node* createNode(float vl, operator op){
    node* temp = (node*)malloc(sizeof(node));
    temp->op = op;
    switch(op){
        case sub: temp->dat = 1; break;
        case add: temp->dat = 1; break;
        case mul: temp->dat = 2; break;
        case dvsn: temp->dat = 2; break;
        case exp: temp->dat = 3; break;
        case pOpen: temp->dat = 4; break;
        case pClose: temp->dat = 4; break;
        case val: temp->dat = vl; break;
    }
    temp->next = NULL;
    temp->prev = NULL;
    return temp;
}

node* appendNode(node* prev, node* target){
    if(prev == NULL)
        return target;
    else{
        prev->next = target;
        target->prev = prev;
        return target;
    }
}

void deleteNode(node* target){
    if(target->prev!=NULL)
        target->prev->next = target->next;
    if(target->next!=NULL)
        target->next->prev = target->prev;

    free(target);
}

node* placeHighPrec(node* toAppend, node* lstApndPos){
    while(lstApndPos->op!=val && (lstApndPos->dat < toAppend->next->dat))
        lstApndPos = lstApndPos->prev;
    toAppend->prev = lstApndPos;
    toAppend->next->next = lstApndPos->next;
    lstApndPos->next->prev = toAppend;
    lstApndPos->next = toAppend;

    return toAppend->next;
}

node* placeEquivPrec(node* toAppend, node* lstApndPos){
    toAppend->prev = lstApndPos;
    toAppend->next->next = lstApndPos->next;
    if(lstApndPos->next)
        lstApndPos->next->prev = toAppend;
    lstApndPos->next = toAppend;

    return toAppend->next;
}

node* placeLowPrec(node* toAppend, node* lstApndPos){
    while(lstApndPos->op!=val && (lstApndPos->dat > toAppend->next->dat)){
        if(lstApndPos->next == NULL){
            lstApndPos->next = toAppend;
            toAppend->prev = lstApndPos;
            toAppend->next->next = NULL;
            return toAppend->next;
        } else lstApndPos = lstApndPos->next;
    }

    toAppend->next = lstApndPos;
    toAppend->prev = lstApndPos->prev;
    lstApndPos->prev->next = toAppend;
    lstApndPos->prev = toAppend;

    return toAppend->next;
}

node* convInfixPostfix(node* head){
    node* nHead = head;
    head = head->next;
    float prevPrior = head->dat;
    node* prevAppend = nHead;
    nHead->next = NULL;
    while(head && head->next){
        node* nextNodes = head->next->next;
        node* toAppend = head->next;
        appendNode(toAppend, head);
        toAppend->prev = NULL;
        toAppend->next->next = NULL;
        if(toAppend->next->dat > prevPrior)
            prevAppend = placeHighPrec(toAppend,prevAppend);
        else if(toAppend->next->dat == prevPrior)
            prevAppend = placeEquivPrec(toAppend,prevAppend);
        else prevAppend = placeLowPrec(toAppend,prevAppend);

        prevPrior = toAppend->next->dat;
        head = nextNodes;
    }
    return nHead;
}

void printExp(node* head){
    while(head){
        switch(head->op){
            case sub: printf("- "); break;
            case add: printf("+ "); break;
            case mul: printf("* "); break;
            case dvsn: printf("/ "); break;
            case exp: printf("^ "); break;
            case pOpen: printf("( "); break;
            case pClose: printf(") "); break;
            case val: printf("%f ", head->dat); break;
        }
        head = head->next;
    }
    printf("\n");
}

node* parseExpression(char* strng){
    int strnglen = strlen(strng);
    node* head = NULL;
    node* curr = head;
    int mulFac = 1;
    char* numStr = NULL;
    int numStrIn = 0;

    for(int i=0; i<strnglen; i++){
        if(isIntOrDot(strng[i])){
            if(numStr == NULL){
                numStr = (char *)malloc(MAXNUMSIZE);
                numStrIn = 0;
                numStr[numStrIn] = strng[i];
            } else
                numStr[++numStrIn] = strng[i];
        } else {
            operator op = identifyOp(strng[i]);
            if(numStr != NULL){
                numStr[++numStrIn] = '\0';
                float vl = strToFloat(numStr);
                curr = appendNode(curr,createNode(vl*mulFac,val));
                if(head == NULL) head = curr;
                free(numStr);
                numStr = NULL;
                mulFac = 1;
                numStrIn = 0;
            }
                curr = appendNode(curr,createNode(0,op));
                if(head == NULL) head = curr;
            }
    }

    if(numStr){
        float vl = strToFloat(numStr);
        curr = appendNode(curr,createNode(vl*mulFac,val));
        if(head == NULL) head = curr;
        free(numStr);
    }

    return head;
}

void sanitizeParsedExp(node** head){
    //Remove unary plus and minus
    if((*head)->next->op == val){
        if((*head)->op == add){
            node* temp = *head;
            *head = (*head)->next;
            deleteNode(temp);
        } else if((*head)->op == sub){
            node* temp = *head;
            (*head)->next->dat *= -1;
            *head = (*head)->next;
            deleteNode(temp);
        }
    }

    node* tmp = (*head)->next;
    while(tmp->next){
        if(tmp->next->op == val ){
            if(tmp->op == add && tmp->prev->op == pOpen){
                node* temp = tmp;
                tmp = tmp->next;
                deleteNode(temp);
            } else if(tmp->op == sub && tmp->prev->op != val){
                node* temp = tmp;
                tmp->next->dat *= -1;
                tmp = tmp->next;
                deleteNode(temp);
            } else tmp = tmp->next;
        }else tmp = tmp->next;
    }

    //Update the priorities.
    float priority = 0;
    node* temp = *head;
    while(temp){
        if(temp->op == pOpen)
            priority += temp->dat;
        else if(temp->op == pClose)
            priority -= temp->dat;
        else if(temp->op != val)
            temp->dat += priority;

        temp = temp->next;
    }

    //Remove the bracketts
    while((*head)->op == pOpen){
        node* temp = (*head);
        (*head) = (*head)->next;
        deleteNode(temp);
    }
    node* tp = (*head);
    while(tp){
        node* tmp = tp->next;
        if(tp->op == pOpen || tp->op == pClose)
            deleteNode(tp);
        tp = tmp;
    }
}

char* takeExpression(int size){
    char* str = (char *)malloc(sizeof(char));
    scanf("%s[^\n]", str);
    return str;
}

int main(){
    char* strng = takeExpression(DEFeXPRsIZE);
    node* parsedExpression = parseExpression(strng);
    printExp(parsedExpression);
    sanitizeParsedExp(&parsedExpression);
    printExp(parsedExpression);
    node* nHead = convInfixPostfix(parsedExpression);
    printExp(nHead);
}
