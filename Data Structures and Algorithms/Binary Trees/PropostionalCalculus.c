#include <stdio.h>
#include <stdlib.h>

typedef enum{not, and, or, true, false} logical;

typedef struct nodeBTTag{
    struct nodeBTTag* leftChild;
    logical op;
    short int val;
    struct nodeBTTag* rightChild;
}nodeBT;

typedef struct nodeSTag{
    logical op;
    short int val;
}nodeS;
