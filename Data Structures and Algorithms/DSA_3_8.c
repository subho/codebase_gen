#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#define MAX_NUM_SIZE 200
#define MAX_NUM      10000
#define MAX_NUM_POW  4

typedef struct num_blocks{
    int num;
    struct num_blocks* next;
}num_block;

typedef struct big_nums{
    int size;
    num_block* msb;
    num_block* lsb;
}big_num;

void push_back(big_num* nmbr, int num){
    num_block* temp = (num_block*)malloc(sizeof(num_block));
    temp->next = NULL;
    temp->num = num;
    if(nmbr->lsb == NULL){
        nmbr->lsb = nmbr->msb = temp;
        nmbr->size = 1;
    }
    else{
        temp->next = nmbr->lsb;
        nmbr->lsb = temp;
        nmbr->size++;
    }
}

void put_front(big_num* number, int num){
    num_block* temp = (num_block*)malloc(sizeof(num_block));
    temp->num = num;
    temp->next = NULL;

    if(number->msb == NULL){
        number->lsb = number->msb = temp;
        number->size = 1;
    }
    else{
        if(num == 0){
            free(temp);
            return;
        }
        number->msb->next = temp;
        number->msb = temp;
        number->size++;
    }
}

void convert_string_num(big_num* nmbr, char* number){
    int num = 0;

    int len_string = strlen(number);

    for(int i=0;i<len_string%MAX_NUM_POW;i++){
        num = num*10 + (*number) - '0';
        number = number+1;
    }
    push_back(nmbr,num);

    num = 0;
    int count = 0;

    while(*number){
        num = num*10+ (*number) - '0';
        count++;
        number = number+1;
        if(count == MAX_NUM_POW){
            push_back(nmbr, num);
            num = 0;
            count = 0;
        }
    }
}

void print_num(big_num b){
    if(b.lsb){
        int num = b.lsb->num;
        b.lsb = b.lsb->next;
        print_num(b);
        printf("%d,",num);
    }
}

big_num add(big_num b1, big_num b2){
    big_num result;
    result.lsb = NULL;
    result.msb = NULL;

    int size = b1.size > b2.size ? b1.size : b2.size;

    int carry = 0;
    int sum   = 0;
    for(int i=0; i<size; i++){
        if(b1.lsb == NULL){
            sum = (b2.lsb->num + carry)%10000;
            carry = (b2.lsb->num + carry)/10000;
            put_front(&result,sum);
            b2.lsb = b2.lsb->next;
        }
        else if(b2.lsb == NULL){
            sum = (b1.lsb->num + carry)%10000;
            carry = (b1.lsb->num + carry)/10000;
            put_front(&result, sum);
            b1.lsb = b1.lsb->next;
        }
        else{
            sum = (b2.lsb->num + b2.lsb->num+ carry)%10000;
            carry = (b2.lsb->num + b2.lsb->num+ carry)/10000;
            put_front(&result, sum);
            b1.lsb = b1.lsb->next;
            b2.lsb = b2.lsb->next;
        }
    }

    return result;
}

big_num constant_mul(big_num b, int num){
    int mul_res = 0;
    int carry   = 0;

    big_num res;
    res.lsb = NULL;
    res.msb = NULL;

    while(b.lsb){
        mul_res = b.lsb->num * num + carry;
        carry   = mul_res/10000;
        mul_res = mul_res%10000;
        put_front(&res, mul_res);
        b.lsb = b.lsb->next;
    }
    put_front(&res, carry);

    return res;
}

big_num mul_bignums(big_num b1, big_num b2){
    big_num result;
    result.lsb = NULL;
    result.msb = NULL;

    for(int i=0; i<b2.size;i++){
        result = constant_mul(result, 10000);
        result = add(result, constant_mul(b1, b2.lsb->num));
        b2.lsb = b2.lsb->next;
    }

    return result;
}

big_num substract(big_num b1, big_num b2){
    num_block* b2_lsb = b2.lsb;
    for(int i=0; i<b2.size; i++){
        
    }
}

void free_mem(big_num b){
    for(int i=0; i<b.size; i++){
        b.msb = b.lsb;
        b.lsb = b.lsb->next;
        free(b.msb);
    }
}

int main(){
    char number[15] = "1234";
    big_num b;
    b.lsb = NULL;
    convert_string_num(&b,number);
    print_num(b);
    printf("\n");

    big_num res;
    res = add(b,b);
    print_num(res);
    printf("\n");

    big_num mres = constant_mul(res, 10000);
    print_num(mres);
    printf("\n");

    big_num b3;
    b3.lsb = NULL;
    convert_string_num(&b3,"128");
    print_num(b3);
    printf("\n");

    big_num b4;
    b4.lsb = NULL;
    convert_string_num(&b4,"128");
    print_num(b4);
    printf("\n");

    res = mul_bignums(b3, b4);
    print_num(res);
    printf("\n");

    free_mem(b);
    free_mem(b3);
    free_mem(b4);

    return 0;

}
