#define MAX_ELEMS 10
#define DEBUG 1


/* Set ADT will have the following members ::
   Array S containing elements
   Total number of terms in the array*/

struct SET{
    float term[MAX_ELEMS];
    int num_terms;
    int iterator;
};

/* The following function will give 1 if the element is     present
                          will give 0 if the element is not present*/

int is_element_of(float x, struct SET S){
    for(int i=0; i<S.num_terms; i++)
        if(S.term[i] == x) return 1;
    return 0;
}

/* The following function checks whether the Set is empty or not
                                      returns 1 if empty else 0*/

int is_empty(struct SET S){
    if(S.num_terms == 0) return 1;
    else return 0;
}

/* This function will return the number of elements in the set*/

int size_SET(struct SET S){
    return S.num_terms;
}

/* This function will return one value of S in each call
   This will give us a value if we haven't reched the end
   else start from beginning*/

float iterate(struct SET *S){
    if(S->iterator < S->num_terms){
        S->iterator += 1;
        return S->term[S->iterator-1];
    }
    else{
        S->iterator = 0;
        return S->term[S->iterator];
    }
}

/* The following function creates a new,
   initially empty set structure       */

void create(struct SET* S){
    for(int i=0; i<MAX_ELEMS; i++)
        S->term[i] = 0;

    S->num_terms = 0;
    S->iterator  = 0;

    return;
}

/* This function will add elements till the set is full */

void add(struct SET *S, float x){
    if(S->num_terms < MAX_ELEMS) {
        if(is_element_of(x,*S)) return;
        S->term[S->num_terms] = x;
        S->num_terms += 1;
    }
    else printf("Max Capacity reached, discarding element :: %f\n",x);

    return;
}

/* This function will add elements from another set */
void add_from(struct SET from_S1, struct SET* to_S2){
    for(int i=0; i<from_S1.num_terms; i++)
        add(to_S2,from_S1.term[i]);
    return;
}

/* Remove element if it is present in the set */

int remove_elem(struct SET* S, float x){
    if(is_element_of(x,*S)){
        int found = 0;
        for(int i=0; i<S->num_terms-1; i++){
            if(found)
                S->term[i] = S->term[i+1];
        }
        S->term[(S->num_terms)-1] = 0;
        S->num_terms -= 1;
        return 1;
    }

    return 0;
}

/* The following function will return max capacity of set */

int capacity(struct SET S){
    return MAX_ELEMS;
}

/* Implementing Set-Theoritic operation of Union */

struct SET Union(struct SET S1, struct SET S2){
    struct SET S;

/*  Creating an element of type SET  */
    create(&S);

    add_from(S1,&S);
    add_from(S2,&S);

    return S;
}

struct SET intersection(struct SET S1, struct SET S2){
    struct SET S;

/* Creating an element of type SET */
    create(&S);

    for(int i=0; i<S1.num_terms; i++)
        if(is_element_of(S1.term[i], S2)) add(&S, S1.term[i]);

    return S;
}

struct SET difference(struct SET S1, struct SET S2){
    struct SET S;

/* Creating an element of type SET */
    create(&S);

    for(int i=0; i<S1.num_terms; i++)
        if(!is_element_of(S1.term[i], S2)) add(&S, S1.term[i]);

    return S;
}
