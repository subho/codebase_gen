#include <stdio.h>
#include <stdlib.h>

#define SENTINEL_VAL 0

typedef struct SM_tag{
    int row;
    int col;
    float val;
    struct SM_tag* next_r; /*Index to element in the next row*/
    struct SM_tag* next_c; /*Index to element in the next col*/
    struct SM_tag* prev_r; /*Index to element in the prev row*/
    struct SM_tag* prev_c; /*Index to element in the prev col*/
} SM_elem;

/*
* In create_head we initialize the data as well as pointer variables.
* As defined in SPARSE MATRIX ADT, the first or head element contains
* information about the number of rows and columns of the ADT
*/
SM_elem* create_head(int rows, int cols){
    SM_elem* head;
    head = (SM_elem*)malloc(sizeof(SM_elem));

    head->row = rows;
    head->col = cols;
    head->val = SENTINEL_VAL;

    head->next_c = NULL;
    head->next_r = NULL;
    head->prev_c = NULL;
    head->prev_r = NULL;

    return head;
}

/*
* In find_row and find_col function, we're applying the following algo ::
    In find_row :
        while(the col is not empty && if we don't reach the col_head again)
            if(the row in that col exists, i.e. has non-zero data)
                return the address of that row element;
            else go to the next row element in the col;

    It is the same for find_col
*/

SM_elem* find_row(SM_elem* col_h, int row){
    SM_elem* col_head = col_h->next_r;
    while(col_head!=NULL && col_head!=col_h){
        if(col_head->row == row) return col_head;
        else col_head = col_head->next_r;
        /*Go to the next element in the same col but next row*/
    }
    return NULL;
}

SM_elem* find_col(SM_elem* row_h, int col){
    SM_elem* row_head = row_h->next_c;
    while(row_head!=NULL && row_head!=row_h) {
        if(row_head->col == col) return row_head;
        else row_head = row_head->next_c;
        /*Go to the next element in the same row but next col*/
    }
    return NULL;
}

SM_elem* add_row(SM_elem* head, int row){
    SM_elem* row_elem;
    row_elem = (SM_elem*)malloc(sizeof(SM_elem));

    row_elem->row = row;
    row_elem->col = 0;
    row_elem->val = SENTINEL_VAL;

    if(head->prev_r == NULL){
        /*
        * forming a circular doubly linked list here
        * row_elem<->head<->row_elem;
        */
        head->next_r = row_elem;
        head->prev_r = row_elem;
        row_elem->prev_r = head;
        row_elem->next_r = head;
        row_elem->next_c = NULL;
        row_elem->prev_c = NULL;
    }
    else{
        /*
        * Check if the last row assigned here is less than given row
        * If true: Directly insert the row_elem there and do the update
        * Else start from head, if it reaches an element with row > given row,
        * insert the row_elem before that.
        */

        SM_elem* last_elem = head->prev_r;

        /*
        * Go to the element which is just less than row, and insert there
         */
        while(last_elem->row > row && last_elem != head)
            last_elem = last_elem->prev_r;


        row_elem->next_r = last_elem->next_r;
        row_elem->prev_r = last_elem;
        last_elem->next_r = row_elem;
        (row_elem->next_r)->prev_r = row_elem;
        row_elem->next_c = NULL;
        row_elem->prev_c = NULL;
    }

    return row_elem;
}

SM_elem* add_col(SM_elem* head, int col){
    SM_elem* col_elem;
    col_elem = (SM_elem*)malloc(sizeof(SM_elem));

    col_elem->row = 0;
    col_elem->col = col;
    col_elem->val = SENTINEL_VAL;

    /*
    * if no column element has been previously assigned
    * Establish the circular doubly linked list here
    */
    if(head->prev_c == NULL){
        head->next_c = col_elem;
        head->prev_c = col_elem;
        col_elem->prev_c = head;
        col_elem->next_c = head;
        col_elem->next_r = NULL;
        col_elem->prev_r = NULL;
    }
    else{
        SM_elem* last_elem = head->prev_c;

        while(last_elem->col>col && last_elem != head)
            last_elem = last_elem->prev_c;

        col_elem->next_c = last_elem->next_c;
        col_elem->prev_c = last_elem;
        last_elem->next_c = col_elem;
        (col_elem->next_c)->prev_c = col_elem;
        col_elem->next_r = NULL;
        col_elem->prev_r = NULL;
    }
    return col_elem;
}

SM_elem* add_elem(SM_elem* head, int row, int col, float val){
    SM_elem* corres_row;
    SM_elem* corres_col;

    SM_elem* elem;
    elem = (SM_elem*)malloc(sizeof(SM_elem));

    elem->row = row;
    elem->col = col;
    elem->val = val;

    corres_row = find_row(head,row);
    corres_col = find_col(head,col);
    if(corres_row == NULL) corres_row = add_row(head, row);
    if(corres_col == NULL) corres_col = add_col(head, col);

    if(corres_row->prev_c == NULL){
        corres_row->next_c = elem;
        corres_row->prev_c = elem;
        elem->prev_c = corres_row;
        elem->next_c = corres_row;
    }
    else{
        SM_elem* last_elem = corres_row->prev_c;

        while(last_elem->col>col && last_elem != corres_row)
            last_elem = last_elem->prev_c;

        elem->next_c = last_elem->next_c;
        elem->prev_c = last_elem;
        last_elem->next_c = elem;
        (elem->next_c)->prev_c = elem;
    }

    if(corres_col->prev_r == NULL){
        corres_col->next_r = elem;
        corres_col->prev_r = elem;
        elem->next_r = corres_col;
        elem->prev_r = corres_col;
    }
    else{
        SM_elem* last_elem = corres_col->prev_r;

        while(last_elem->col>col && last_elem != corres_col)
            last_elem = last_elem->prev_r;

        elem->next_r = last_elem->next_r;
        elem->prev_r = last_elem;
        last_elem->next_r = elem;
        (elem->next_r)->prev_c = elem;
    }

    return elem;

}

void display(SM_elem* head){
    SM_elem* t_row = head;
    SM_elem* t_col = head;

    do{
        t_col = t_row;
        do{
            printf("add= %p row= %d col= %d ",t_col,t_col->row, t_col->col);
            printf("val= %f prev_r= %p prev_c= %p ",t_col->val,t_col->prev_r,t_col->prev_c);
            printf("next_r= %p next_c= %p\n",t_col->next_r,t_col->next_c);
            t_col = t_col->next_c;
        }while(t_col != t_row);
        t_row = t_row->next_r;
    }while(t_row != head);
}



int main()
{
    int rows, cols, num_elems;

    printf("Enter rows, cols, num_elems :: ");
    scanf("%d",&rows);
    scanf("%d",&cols);
    scanf("%d",&num_elems);

    SM_elem* head;
    head = create_head(rows,cols);

    int row, col, val;
    for(int i=0; i<num_elems; i++){
        printf("Enter %d. row col val ::",i);
        scanf("%d",&row);
        scanf("%d",&col);
        scanf("%d",&val);
        printf("%p\n",add_elem(head,row,col,val));
    }
    display(head);

    return 0;
}
