#include <stdio.h>
#include <stdlib.h>

int main()
{
	int *arr;
	int n;
	
	printf("Enter the num of elements to input:: ");
	scanf("%d[^\n]", &n);
	
	arr=(int *)malloc(n*sizeof(int));
	
	printf("enter the elements now :: \n");
	for(int i=0;i<n;i++)
		scanf("%d[^\n]", arr+i);
	
	printf("The elements are as follows: \n");
	for(int i=0;i<n;i++)
		printf("....%d\n", *(arr+i));
		
	return 0;
}		
