#include <stdio.h>
#include <stdlib.h>

#define MAX_CHAR_NAME 31

struct student{
	int roll;
	char name[MAX_CHAR_NAME];
	int net_score;
	struct student* next;
};

void get_std_data(struct student* s)
{
	printf("Enter roll of student: ");
	scanf("%d", &(s->roll));
	printf("Enter the name of the student: ");
	scanf("%s", &(s->name));
	printf("Enter the net_score: ");
	scanf("%d", &(s->net_score));

	s->next = NULL;
	return;
}

void create(struct student** head)
{
	struct student* s;
	s = (struct student*)malloc(sizeof(struct student));

	get_std_data(s);

	*head = s;
	return ;
}

void append(struct student* head)
{
	struct student* s;
	s = (struct student*)malloc(sizeof(struct student));

	get_std_data(s);
	
	while(head->next!=NULL)
		head = head->next;

	head->next = s;
	return;
}

void delete(int roll, struct student** head)
{
	struct student *prev, *curr;
	prev = *head;
	if(*head->roll == roll){
		*head = *head->next;
}
		
	

	
