#include <iostream>
#include <string>
#include <vector>

using namespace std;

#define SENTINEL_MARKS -1
#define MAX_SUBS        5

class Student{
private:
    int roll;
    string name;
    string ph_no;
    float subs[MAX_SUBS];

    static vector<vector<int>> subjects;
public:
    Student(int = 0,string = "\0",string = "\0");
    void select_subs();
    void enter_scores();
    void show_data();

    static void show_roll(string);
};

Student::Student(int rl, string nm, string pno){
    roll = rl;
    name = nm;
    ph_no = pno;

    for(int i=0; i<MAX_SUBS; i++)
        subs[i] = 0;
}

void Student::select_subs(){
    for(int i=0; i<MAX_SUBS; i++){
        cout<<i+1<<". Do you want sub"<<i+1<<"?\n";
        cout<<"Enter Y for yes and N for no :: ";
        char c;
        cin>>c;
        if(c == 'Y'){
            if(subjects.size() < i+1){
                vector<int> temp;
                subjects.push_back(temp);
            }
            subjects[i].push_back(roll);
        }
        else if(c == 'N') subs[i] = SENTINEL_MARKS;
    }
    return;
}

void Student::enter_scores(){
    int marks;
    for(int i=0; i<MAX_SUBS; i++){
        if(subs[i] == SENTINEL_MARKS)
            continue;
        else{
            cout <<"Enter the marks for sub"<<i+1<<" ::";
            cin>>marks;
            subs[i] = marks;
        }
    }
}

void Student::show_data(){
    cout<<name<<"\n"<<roll<<"\n"<<ph_no<<"\n";
    for(int i=0; i<MAX_SUBS; i++){
        if(subs[i] != SENTINEL_MARKS){
            cout<<"Marks for sub"<<i<<":: ";
            cout<<subs[i]<<"\n";
        }
    }
}

void Student::show_roll(string sub){
    if(sub == "sub1"){
        for(int i=0; i<subjects[0].size(); i++){
            cout<<subjects[0][i]<<"\n";
            return;
        }
    }
    else if(sub == "sub2"){
        for(int i=0; i<subjects[1].size(); i++){
            cout<<subjects[1][i]<<"\n";
            return;
        }
    }
    else if(sub == "sub3"){
        for(int i=0; i<subjects[3].size(); i++){
            cout<<subjects[3][i]<<"\n";
            return;
        }
    }
    else if(sub == "sub4"){
        for(int i=0; i<subjects[3].size(); i++){
            cout<<subjects[3][i]<<"\n";
            return;
        }
    }
    else if(sub == "sub5"){
        for(int i=0; i<subjects[4].size(); i++){
            cout<<subjects[4][i]<<"\n";
            return;
        }
    }

    cout<<"No such subject \n";
    return;
}
