#include <iostream>
#include <string>
#include <vector>
using namespace std;

class APPLICANT{
private:
    string roll;
    string name;
    float score;
    int ap_id;

    static int prev_id;
    static int application_count;

public:
    APPLICANT();
    void get_details();
    void show_details();
    static int applicant_count();
};

int APPLICANT::prev_id = 0xfff;
int APPLICANT::application_count = 0;

APPLICANT::APPLICANT(){
    roll = "\0";
    name = "\0";
    score = 0.0;
    ap_id = 0;
    application_count ++;
    prev_id ++;
}

void APPLICANT::get_details(){
    cout <<"Enter the 8 digit roll :: ";
    cin  >> roll;

    cout <<"Enter name             :: ";
    cin >> name;

    cout <<"Enter the score         :: ";
    cin >> score;

    ap_id = prev_id+1;
}

void APPLICANT::show_details(){
    cout <<"Name is :: " <<name<<"\n";
    cout <<"Roll is :: " <<roll<<"\n";
    cout <<"Score    = " <<score<<"\n";
    cout <<"Application id :: "<<ap_id<<"\n";
}

int APPLICANT::applicant_count(){
    return application_count;
}

int main(){
    int choice = 0;
    vector<APPLICANT> studs;
    cout <<": Press 1 to enter new data   ::\n"
         <<": Press 2 to show details ::\n"
         <<": Press 3 to show applicant count ::\n"
         <<": Press 4 to exit _||_\n";

    do{
        cout<<"Express choice :: ";
        cin >>choice;

        APPLICANT temp;

        switch(choice){
            case 1: cout <<"Entering details :: \n";
                    temp.get_details();
                    studs.push_back(temp);
                    break;
            case 2: if(studs.size() == 0)
                        cout<<"No data entered !! enter data\n";
                    else
                        cout<<"Showing details :: \n";
                        studs[studs.size()-1].show_details();
                    break;
            case 3: cout <<"Applicant count == "<<APPLICANT::applicant_count()<<"\n";
                    break;
            case 4: cout <<"Terminating process (*|*)\n";
                    break;
            default: cout <<"Wrong choice ##%@\n";
                     break;
        }
    }while(choice != 3);

    return 0;
}
