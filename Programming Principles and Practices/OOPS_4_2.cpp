#include <iostream>
using namespace std;

class Complex{
private:
    float rl;
    float imz;
public:
    Complex();
    Complex(float);
    Complex(float,float);
    void show_val();
    void set_val(float, float);
    Complex operator +(const Complex &C);
    friend Complex operator -(Complex C1, Complex C2);
    friend ostream& operator <<(ostream& T, Complex C);
};

Complex::Complex(){
    rl = 0;
    imz = 0;
}

Complex::Complex(float r){
    rl = r;
    imz = 0;
}

Complex::Complex(float r, float im){
    rl = r;
    imz = im;
}


void Complex::show_val(){
    cout<<rl<<"+"<<imz<<"i";
}

void Complex::set_val(float r, float im){
    rl = r;
    imz = im;
}

Complex Complex::operator+(const Complex& C){
    Complex C_tmp(0,0);
    C_tmp.rl = C.rl + rl;
    C_tmp.imz = C.imz + imz;
    return C_tmp;
}

Complex operator -(Complex C1, Complex C2){
    Complex C_tmp(0,0);
    C_tmp.rl = C1.rl - C2.rl;
    C_tmp.imz = C1.imz - C2.imz;
    return C_tmp;
}

ostream& operator <<(ostream& T, Complex C){
    T<<C.rl;
    if(C.imz >= 0) T<<"+";
    else {
        T<<"-";
        C.imz = -1*C.imz;
    }
    T<<C.imz<<"i";
    return T;
}

int main(){
    Complex C1;
    float r,im;

    cout<<"Enter the values of R and Imz part for C1 :: ";
    cin >> r>>im;
    C1.set_val(r, im);
    cout<<C1<<"\n";

    cout<<"Taking the default values of R =5 and Imz =10 for C2 :: ";
    Complex C2(5,10);
    cout<<C2<<"\n";

    Complex C3,C4;
    cout<<"Here's the addition value of C1 and C2 :: ";
    C3 = C1+C2;
    cout<<C3<<"\n";

    cout<<"Here's the result of C1 - C2 :: ";
    C4 = C1 - C2;
    cout<<C4<<"\n";

    cout<<"Here's the result of C2 - C1 :: ";
    C4 = C2 - C1;
    cout<<C4<<"\n";

    Complex C5;
    C5 = 10;
    cout<<C5 + 3.1;

    return 0;
}
