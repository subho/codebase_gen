#include <iostream>
using namespace std;

#define SENTINEL_tx_rate 0

float tax_return(float income, float tax_rate = 0.1){
	return income*tax_rate;
}

int main(){
	float income;
	float tax_rate;
	cout <<"Income = ";
	cin >> income;
	
	cout <<"Enter "<<SENTINEL_tx_rate<<"for default taxation. Else"
	       <<" enter tax rate = ";
	cin >> tax_rate;

	cout<<"Tax Return = ";
	if(tax_rate == SENTINEL_tx_rate) cout<<tax_return(income) <<"\n";
	else cout<<tax_return(income,tax_rate/100) <<"\n";

	return 0;
}


