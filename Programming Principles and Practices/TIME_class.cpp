#include <iostream>
using namespace std;

class TIME{
	int secs;
	int mins;
	int hours;
	public:
		/*Managerial functions*/
		TIME();
		~TIME();
		/*MutATOR functions*/
		void set_Time_24(int,int,int);
		void set_Time_AP(int,int,int,char*);
		void add_min(int,TIME);
		/*Accessor functions*/
		void show_Time_24(void);
		void show_Time_AP(void);
}

TIME::TIME(){
	secs = 0;
	mins = 0;
	hours = 0;
}

void TIME::set_time_24(int h, int m, int s){
	secs = s;
	mins = m;
	hours = h;
	
	return;
}

void TIME::set_time_AP(int h, int m, int s, char tag[2]){
	if(tag == "PM")
		h = h+12;
	secs = s;
	mins = m;
	hours = h;

	return;
}

void TIME::add_min(int m, TIME t){
	t.m = t.m + m;
	if(t.m/60 != 0){
		t.h = t.m/60;
		t.m = t.m%60;
	}

	return;
}

void TIME::show_Time_24(void){
	cout<<"hours = "<<hours<<"\n";
	cout<<"mins = "<<mins<<"\n";
	cout<<"secs = "<<secs<<"\n";

	return;
}

void TIME::show_Time_AP(void){
	if(hours == 0) cout<<"hours = 12 AM"<<"\n";
	else if(hours == 12) cout<<"hours = 12 PM\n";
	else if(hours > 12) cout<<"hours = "<<hours-12<<"PM\n";
	else cout<<"hours = "<<hours<<"AM\n";
	cout<<"mins = "<<mins<<"\n";
	cout<<"secs = "<<secs<<"\n";

	return;
}

int main(){
	TIME t;
	int s, m, h;
	char format[2];
	cout<<"hour = "<<





			 

