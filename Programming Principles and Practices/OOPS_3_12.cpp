#include <iostream>
using namespace std;

class node{
private:
    float val;
    node* next;
    node* prev;
public:
    node();
    friend class List;
};

node::node(){
    prev = NULL;
    next = NULL;
    val  = 0;
}

class List{
private:
    node* head;
    node* curr;
    node* create_elem(float);
public:
    /* Managerial functions*/
    List();
    List(const List&);
    ~List();

    /*Mutator functions*/
    bool del_front();
    bool del_after(node*);
    bool insert_after(node*, float);
    bool insert_front(float);
    void goto_next_elem();
    void goto_prev_elem();
    void reset_curr();

    /*Accessor functions*/
    bool at_end() const;
    bool is_empty() const;
    node* curr_elem() const;
    float ret_val(node*) const;
};

List::List(){
    head = NULL;
    curr = NULL;
}

List::List(const List& s){
    node* t = s.head;
    if(t == NULL) return;
    insert_front(t->val);
    t = t->next;
    curr = head;
    while(t != NULL){
        insert_after(curr,t->val);
        goto_next_elem();
        t = t->next;
    }
}

List::~List(){
    node* tmp;
    while(head != NULL){
        tmp = head;
        head = head->next;
        delete tmp;
    }
}

node* List::create_elem(float val){
    node* temp = new node();
    if(temp != NULL)
        temp->val = val;
    return temp;
}

bool List::insert_after(node *temp,float val){
    node* tmp = create_elem(val);
    if(tmp == NULL || temp == NULL) return false;

    curr = temp;
    tmp->next = temp->next;
    if(temp->next != NULL) temp->next->prev = tmp;
    temp->next = tmp;
    tmp->prev  = temp;
    return true;
}

bool List::insert_front(float val){
    node* tmp = create_elem(val);

    if(tmp == NULL) return false;
    else if(head == NULL) head = tmp;
    else{
            tmp->next = head;
            head->prev = tmp;
            head = tmp;
    }

    curr = head;
    return true;
}

bool List::del_front(){
    if(head == NULL) return false;
    else{
        node* tmp = head;
        head = head->next;
        head->prev = NULL;
        delete tmp;
        curr = head;
        return true;
    }
}

bool List::del_after(node* elem){
    if(elem->next == NULL) return false;
    else{
        curr = elem;
        node* tmp = elem->next;
        elem->next = tmp->next;
        if(tmp->next != NULL) tmp->next->prev = elem;
        delete tmp;
        return true;
    }
}

bool List::at_end () const{
    if(curr->next == NULL) return true;
    else return false;
}

bool List::is_empty() const{
    if(head == NULL) return true;
    else return false;
}

void List::goto_next_elem(){
    curr = curr->next;
    return;
}

void List::goto_prev_elem(){
    curr = curr->next;
    return;
}

void List::reset_curr(){
    curr = head;
}

node* List::curr_elem() const{
    return curr;
}

float List::ret_val(node* elem) const{
    return elem->val;
}

int main(){
    int num = 0;
    cout <<"Enter number of elements :: ";
    cin >> num;

    if(num == 0){
        cout<<"You entered 0 elementss ???  :)\n";
        return 0;
    }

    List l;

    /* Element that takes the input*/
    float n;
    cout << "Enter "<<num<<" elements here (of type float) :: \n";
    for(int i=0; i<num; i++){
        cin >> n;
        if(l.is_empty()) l.insert_front(n);
        else {
            l.insert_after(l.curr_elem(),n);
            l.goto_next_elem();
        }
    }

    l.reset_curr();
    l.goto_next_elem();
    l.insert_after(l.curr_elem(),4);
    cout <<"Here here are the numbers :: \n";
    l.reset_curr();
    while(1){
        if(!l.at_end()){
            cout<<l.ret_val(l.curr_elem())<<" <----> ";
            l.goto_next_elem();
        }
        else{
            cout<<l.ret_val(l.curr_elem())<<"\n";
            break;
        }

    }

    List s = l;
    cout <<"Here here are the numbers :: \n";
    s.reset_curr();
    while(1){
        if(!s.at_end()){
            cout<<s.ret_val(s.curr_elem())<<" <----> ";
            s.goto_next_elem();
        }
        else{
            cout<<s.ret_val(s.curr_elem())<<"\n";
            break;
        }

    }
    return 0;
}
