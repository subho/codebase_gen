#include <iostream>
using namespace std;

#define SUB_NAME_LEN 10
#define SUB_CODE_LEN 10
#define NAME_LEN     10

class subject{
    char sub_name[SUB_NAME_LEN];
    char sub_code[SUB_CODE_LEN];
public:
    subject();
    void get_info();
};

class sublist{
    int num_subs;
    subject* sub_list;
public:
    sublist();
    void get_list();
    ~sublist();
};

class student{
    char name[NAME_LEN];
    int roll;
    static int last_roll;
public:
    student();
    void get_info();
};

class studlist{
    int num_studs;
    student* stud_list;
public:
    studlist();
    void get_list();
    ~studlist();
};

class map_stud_sub{
    
}
