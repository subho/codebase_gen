#include <iostream>
using namespace std;

class STRING{
    char* char_arr;
    int sz;
    int compare(STRING, STRING) const;
public:
    STRING(char* = NULL);
    STRING& operator=(const STRING&);
    STRING operator+(const STRING&);
    bool operator>(STRING&) const;
    bool operator==(STRING&)const;
    bool operator<(STRING&) const;
    friend ostream& operator<<(ostream& , const STRING&);
    ~STRING();
};

STRING::STRING(char* to_assign){
    sz = 0;
    if(to_assign == NULL){
        char_arr = NULL;
        sz = 0;
    }
    else{
        while(to_assign[sz]!='\0')
            sz++;
        sz = sz+1;
        char_arr = new char[sz];

        for(int i=0; i<sz; i++)
            char_arr[i] = to_assign[i];
    }
}

STRING& STRING::operator=(const STRING& str){
    if(sz != 0){
        delete[] char_arr;
        sz = 0;
    }

    sz = str.sz;
    char_arr = new char[sz];

    for(int i=0; i<sz; i++)
        char_arr[i] = str.char_arr[i];


    return *this;
}

STRING STRING::operator+(const STRING& temp){
    STRING add_val;
    add_val.sz = sz + temp.sz-1;

    add_val.char_arr = new char[add_val.sz];
    for(int i=0; i<add_val.sz; i++){
        if(i>=sz-1)
            add_val.char_arr[i] = temp.char_arr[i-sz+1];
        else add_val.char_arr[i] = char_arr[i];
    }
    return add_val;
}

int STRING::compare(STRING S1, STRING S2) const{
    int i = 0;
    while(i<S1.sz && i<S2.sz){
        if(S1.char_arr[i] == S2.char_arr[i]){
            i++;
            continue;}
        else if(S1.char_arr[i] > S2.char_arr[i])
            return 1;
        else return -1;
        i++;
    }
    if(S1.sz == S2.sz) return 0;
    else if(S1.sz < S2.sz) return -1;
    else return 1;
}

bool STRING::operator<(STRING& s) const{
    if(compare(*this,s) == -1) return true;
    else return false;
}

bool STRING::operator == (STRING& s) const{
    if(compare(*this,s) == 0) return true;
    else return false;
}

bool STRING::operator>(STRING& s) const{
    if(compare(*this,s) == 1) return true;
    else return false;
}

ostream& operator<<(ostream& t,const STRING& s){
    t<<s.char_arr;
    return t;
}

STRING::~STRING(){
    if(sz != 0)
        delete[] char_arr;
        sz = 0;
}

int main(){
    char* s1 = new char[10];
    char* s2 = new char[12];
    char* s3 = new char[6] ;

    for(int i=0; i<10; i++)
        s1[i] = i+'a';

    for(int j=0; j<12; j++)
        s2[j] = 'z'- j;

    for(int k=0; k<6; k++)
        s3[k] = 'l' + k;

    STRING S1 = s1;
    STRING S2 = s2;

    cout<<S1<<"\n";
    cout<<S2<<"\n";

    STRING S3;
    S3 = S1 + S2;
    cout<<"\n\n"<<S3<<"\n";

    if(S1 == S1) cout<<"OK1\n";
    if(S1 < S2)  cout<<"OK2\n";
    if(S3 < S3)  cout<<"OK3\n";
    else cout<<"Wrong \n";
    return 0;
}
