#include <iostream>
#include <string>
using namespace std;

#define NUM_SUBS 5
#define SENTINEL_MARKS -1

class d{
    int year;
    int month;
    int day;
public:
    d();
    void update_date();
    void show_date();
};

d::d(){
    year  = -1 ;
    month = -1 ;
    day   = -1 ;
}

void d::update_date(){
    cout<<"Enter Date in this format d/m/y for date/n";

    cin >> day;
    while(day>31 || day < 1){
        cout<<"wrong date!! Enter again\n";
        cin>>day;
    }

    char c;
    cin >>c;
    cin>>month;
    while(month>12 || month < 1){
        cout<<"wrong month!! Enter again\n";
        cin>>month;
    }

    cin>>c;
    cin>>year;
    while(year < 0){
        cout<<"wrong year!! Enter again\n";
        cin>>year;
    }
}

void d::show_date(){
    if(day == -1){
        cout<<"Till now, data is not entered \n";
        return;
    }
    cout<<"Date is :: ";
    cout<<day<<"/"<<month<<"/"<<year<<"\n";

    return;
}

class Student{
    int roll;
    string name;
    string course;
    d date;
    float sub_marks[5];

    static int count;
public:
    Student(int=0,string="\0",string="\0");
    void set_details();
    void update_marks();
    float ret_marks();
    void show_details();
    static void show_count();
};

int Student::count = 0;

void Student::show_count(){
    cout<<"Count is :: "<<count<<"\n";
    return;
}

Student::Student(int rl, string nm, string crse){
    roll = rl;
    name = nm;
    course = crse;
    count ++;
    for(int i=0; i<NUM_SUBS; i++)
        sub_marks[i] = SENTINEL_MARKS;
}

void Student::set_details(){
    cout<<"Enter the roll no. here :: ";
    cin >>roll;

    cout<<"Enter the name          :: ";
    cin >>name;

    cout<<"Enter the course name   :: ";
    cin >>course;

    cout<<"Enter today's date      :: \n";
    date.update_date();
}

void Student::update_marks(){
    cout<<"Please enter your marks below ::\n";
    for(int i=0;i<NUM_SUBS; i++){
        cout<<"Enter the marks for sub"<<i+1<<" :: ";
        cin>>sub_marks[i];
    }
}

float Student::ret_marks(){
    cout<<"!Press 1 for subject 1   ||\n"
        <<"!Press 2 for subject 2   ||\n"
        <<"!Press 3 for subject 3   ||\n"
        <<"!Press 4 for subject 4   ||\n"
        <<"!Press 5 for subject 5   ||\n";

    cout<<"Enter choice :: ";

    int choice;
    cin>>choice;

    if(choice <=5 && choice >= 1)
        return sub_marks[choice];
    else{
        cout<<"No subject as such :-(\n";
        return SENTINEL_MARKS;
    }
}

void Student::show_details(){
    cout<<"Name of the candidate :: "<<name<<"\n";
    cout<<"Roll no.              :: "<<roll<<"\n";
    cout<<"Course name is        :: "<<course<<"\n";
    cout<<"Admission ";
    date.show_date();

    for(int i=0;i<NUM_SUBS; i++){
        cout<<"Marks for sub"<<i+1<<" is :: ";
        cout<<sub_marks[i]<<"\n";
    }
}

int main(){
    Student s_array[3];

    for(int i=0; i<3; i++){
        s_array[i].set_details();
        s_array[i].update_marks();
        Student::show_count();
    }

    for(int i=0; i<3; i++){
        s_array[i].show_details();
    }

    for(int i=0; i<3; i++)
        cout<<"Returned marks is \\||/\n"<<s_array[i].ret_marks()<<"\n";

    return 0;
}
