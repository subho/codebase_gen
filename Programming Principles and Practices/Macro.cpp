#include<iostream>
using namespace std;

#define IS_GREATER(x,y) (((x)>(y))?(x):(y))

int main(){
	cout<<"Greater of nums 1 and 2 :: "<<IS_GREATER(1,2)<<"\n";

	cout<<"Greater of strings 'dog' and 'cat' :: "
	    <<IS_GREATER("cat","dog")<<"\n";

	cout<<"Greater of strings 'dog' and 'cat' :: "
	    <<IS_GREATER("dog","cat")<<"\n";
	/*Observation :: This above comparison
	 always select the word with higher net ASCII value*/
}
