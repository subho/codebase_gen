#include <iostream>
using namespace std;

#define MAX_LIBRARY_BOOKS    100
#define MAX_DUPLICATE_COPIES 5
#define MAX_MEMBERSHIPS      50
#define STUDENT_MAX_BOOKS    2
#define FACULTY_MAX_BOOKS    10

/***********************************************************
* Book ::                                                  *
* 1. A book must have book_id, author name, publisher name *
*    and it's price tag.                                   *
* 2. The book's attributes should be fixed at the time of  *
*    of creation of this object. Those attributes should   *
*    be immutable.                                         *
* 3. Public interfaces ::                                  *
*       a. Should provide basic access to the various ids  *
*       b. A function to display the value in those fields *
***********************************************************/

class book{
    char book_id[10];
    char author [15];
    char publisher[15];
    float price;
public:
    book();
    char* get_book_id();
    char* get_author_name();
    char* get_publisher_name();
    float get_price();
    void display_det();
};

/***********************************************************
* Library_book ::                                          *
* Now, for the benifit of better Management the Libraries  *
* modify the books. Thus, this is a case of inheritance. We*
* inherit the fields in private mode because the Library   *
* should not modify any of the field of the book. Instead, *
* it should add an extra field :: the unique serial number *
* to track the books properly. Also, since the fields will *
* be used exclusively by Library, make it a friend of      *
* Library class. Have no public interface here.            *
***********************************************************/

class Library_book:public book{
    char sl_no [10];
public:
    friend class Library;
};

class date{
    int day;
    int month;
    int year;
public:
    date();
    void set_date(int, int, int);
    void compare_dates(date, date);
    void reset_date();
};

/***********************************************************
* Member Details:                                          *
* Contains all information about the members in Library.   *
* 1. Should have the following fields :                    *
*       a. Category of Member ::                           *
*           i. 'S' -> For students                         *
*          ii. 'F' -> For Faculty                          *
*       b. Name of the member                              *
*       c. The email-id of the member                      *
*       d. The unique member-id that will be assigned by   *
*          Library                                         *
*       e. Number of books currently in possesion of the   *
*          member, their return date and the serial number *
*          of book that is issued.                         *
* Since this details are controlled solely by the Library, *
* and solely for the use of Library, declare Library as    *
* friend class. No public interface is necessary. Library  *
* will publish the details if necessary ever.              *
***********************************************************/

class member_detail{
    char member_type;
    char member_name[15];
    char email[15];
    char member_id[15];
    int num_books_issued;
    date* return_dates;
    char* books_issued[10];
public:
    friend class Library;
    ~member_detail();
};

/***********************************************************
* Issued Book ::                                           *
* 1. This is like the slip that library gives to the       *
*    student for referral.                                 *
* 2. Thus the fields of Issued Book should be modifiable   *
*    only by Library Class.                                *
* 3. private members ::                                    *
*       a. serial number of the book                       *
*       b. date of return of the books                     *
* 4. public members ::                                     *
*       a. Interface to display details                    *
***********************************************************/

class Issued_Book{
    char sl_no[10];
    date date_of_return;
public:
    void get_details();
    friend class Library;
};

/***********************************************************
* Person ::                                                *
* 1. Should contain generic information like ::            *
*       a. The name                                        *
*       b. The mail address                                *
*       c. The date of birth                               *
*       d. The address                                     *
* Also the class should be abstract, since it should only  *
* act as base class                                        *
***********************************************************/

class Person{
protected:
    char name[15];
    date date_of_birth;
    char mail_id[15];
    char address[20];
public:
    void virtual set_details()=0;
};

/***********************************************************
* Student ::                                               *
* On top of the basic details that any person should have, *
* a student should have the following details  ::          *
* 1. Date of admission                                     *
* 2. Admission Identification code given during admission  *
* 3. The member id for accessing Library                   *
* 4. A list containing details of books borrowed           *
* 5. Public interfaces :                                   *
*       a. A way to update the student's profile           *
*       b. Recieve a member-id from Library                *
*       c. A way to issue and return books                 *
************************************************************/


class Student:protected Person{
    date admission_date;
    char admission_id[10];
    char member_id[10];
    Issued_Book book_list[STUDENT_MAX_BOOKS];
public:
    void set_details();
    void get_member_id();
    void issue_book();
    void return_book();
};

/***********************************************************
* Faculty ::                                               *
* On top of the basic details that any person should have, *
* a faculty should have the following details  ::          *
* 1. Date of joining the institution                       *
* 2. The employee id                                       *
* 3. The member id for accessing Library                   *
* 4. A list containing details of books borrowed           *
* 5. Public interfaces :                                   *
*       a. A way to update the student's profile           *
*       b. Recieve a member-id from Library                *
*       c. A way to issue and return books                 *
************************************************************/

class Faculty:protected Person{
    date date_of_joining;
    char  employee_id[10];
    char  member_id[10];
    Issued_Book book_list[FACULTY_MAX_BOOKS];
public:
    void set_details();
    void get_member_id();
    void issue_book();
    void return_book();
};

/***********************************************************
* Library::                                                *
* This class does the following functions ::               *
* 1. Maintain a collection of books                        *
*       a. Able to add books to it's collection            *
*       b. Able to sanction these books based on their     *
*          availability                                    *
*       c. Assign serial numbers to the books              *
*       d. Take back books lent and check if the member is *
*          eligible for fine or not                        *
* 2. Maintain a list of people who can access the Library  *
*       a. Issue membership ids and member details for it's*
*          own use.                                        *
*       b. Sanction books based on Maximum alloted books   *
*          for that class of people                        *
*       c. Issue issued_book slips for the person borrowing*
* 3. Public interfaces :                                   *
*       a. add books to the collection                     *
*       b. display the whole list                          *
*       c. New registrations                               *
*       d. Issue books                                     *
*       e. Take back lent books                            *
***********************************************************/

class Library{
    Library_book Collection[MAX_LIBRARY_BOOKS][MAX_DUPLICATE_COPIES];
    int is_available[MAX_LIBRARY_BOOKS][MAX_DUPLICATE_COPIES];
    member_detail Members [MAX_MEMBERSHIPS];
    /* Give the serial_nums for every books in bookshelf*/
    void set_serial_num();
public:
    /* Add a book to the shelf */
    void add_books(book);
    /* Display all the books */
    void display_books();
    /* Name, Email, type and admission_no/faculty_id has to be passed
       as arguments. Returns the member id */
    char* register_members(char[],char[],char, char[]);
    /*Give the member_id and book name here*/
    Issued_Book issue_book(char[], char[]);
    /*Give the Issued_Book ang member id as arguments
      Returns true if returned within return date, else return false*/
    bool return_book(char[], Issued_Book&);
};

void Library::set_serial_num(){
    Collection[0][0].sl_no = "ooo000000";
}

int main(){
    Student s;
}
