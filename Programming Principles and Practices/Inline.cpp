#include <iostream>
#include <string>
using namespace std;

inline int is_Greater(int a, int b){
	return a>b?a:b;	
}

inline string is_Greater(string a, string b){
	if(a>b) return a;
	else return b;
}

int main(){
	cout<<"The greater of nums 1 and 2 :: "
	    <<is_Greater(1,2)<<"\n";
	cout<<"The greater of 2 strings 'cat'and 'dog' :: "
	    <<is_Greater("cat","dog") <<"\n";
	
	return 0;
}
