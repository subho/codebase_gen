#include <iostream>
using namespace std;

int& max(int &a, int &b){
	return a>b?a:b;
}

int main(){
	int a, b; /*Variables where we'll take input*/
	int x;    /*Variable which stores the value of max int */
	
	
	cout<<"Enter values of 'a' and 'b' :: ";
	cin >> a>>b;
	
	x = max(a,b);	
	
	cout<<"Values of 'a' and 'b' :: "<<a<<"\t"<<b<<"\n";
	cout <<"Value of x ::"<<x++<<"\n";

	cout<<"Modifying x...\n";
	cout<<"Values of 'a' and 'b' :: "<<a<<"\t"<<b<<"\n";
	cout <<"Value of x ::"<<x<<"\n\n";
	
	int &y = max(a,b);   /*Variable which stores reference to the 
				max int*/

	cout <<"Value of y ::"<<y++<<"\n";

	cout<<"Modifying y...\n";
	cout<<"Values of 'a' and 'b' :: "<<a<<"\t"<<b<<"\n";
	cout <<"Value of y ::"<<y<<"\n";

	return 0;
}

