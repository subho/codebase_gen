#include <iostream>
#include <cstring>
using namespace std;

#define MAX_NAME_SIZE 20

class date{
    int day;
    int month;
    int year;
public:
    void set_date(int, int, int);
    void compare_dates(date, date);
    void reset_date();
};

class Cricketer{
protected:
    char name[MAX_NAME_SIZE];
    date date_of_birth;
    int num_matches_played;
public:
    virtual void update_details()=0;
};

class Batsman:virtual public Cricketer{
protected:
    int total_runs_scored;
    float average_score;
public:
    void update_details();
    void print_details();
};

class Bowler:virtual public Cricketer{
protected:
    int num_wickets;
    float avrg_economy;
public:
    void update_details();
    void print_details();
};

class AllRounder:public Bowler, public Batsman{
public:
    void update_details();
    void print_details();
};

int main(){
    Batsman b;
    b.update_details();
    b.print_details();
}
