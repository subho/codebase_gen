#include <iostream>
using namespace std;

void swap(int& a, int& b){
	int temp;
	temp = a;
	a = b;
	b = temp;

	return;
}

int main(){
	int a;
	int b;

	cout <<"Enter value of 'a' and 'b' :: ";
	cin >> a>>b;

	cout <<"Swapping values of 'a' and 'b' --- \n";

	swap(a,b);

	cout <<"a = "<<a<<"\tb = "<<b<<"\n";
	
	return 0;
}
