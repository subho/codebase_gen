#include <iostream>
using namespace std;

class Array{
private:
    float* head;
    int sz;

public:
    Array(int);
    Array(const Array& A);
    Array(const float[], int s);
    // This returns reference of an array element as well
    // And thus can be used as an lvalue.
    float& operator[] (int);
    Array operator+(const Array&);
    Array operator=(const Array&);
    friend Array operator*(const Array A1, int i);
    friend Array operator*(int i, const Array A2);
    ~Array();
};

Array::Array(int s){
    sz = s;
    head = new float[s];

    for(int i=0; i<s; i++)
        head[i] = 0;
}

Array::Array(const Array& A){
    sz = A.sz;
    head = new float[sz];

    for(int i=0; i<sz; i++)
        head[i] = A.head[i];
}

Array::Array(const float A[], int s){
    sz = s;
    head = new float[sz];

    for(int i=0; i<s; i++)
        head[i] = A[i];
}

float& Array::operator[](int i){
    if(i<sz && i>= 0)
        return head[i];
    else{
        cout<<":: Index out of bounds :: \n";
        return head[0];
    }
}

Array Array::operator+(const Array& A){
    if(sz != A.sz){
        Array a(0);
        return a;
    }
    else{
        Array a(sz);
        for(int i=0; i<sz; i++)
            a[i] = head[i] + A.head[i];
        return a;
    }
}

Array::~Array(){
    delete[] head;
}

int main(){
    return 0;
}
