#include <iostream>
#include <vector>
using namespace std;

class book{
    string book_name;
    string author;
    string book_id;
    string price;
public:
    void set_book_details();
    string get_book_id();
    void  print_details();
};

void book::set_book_details(){
    cout << "Enter book_name :: ";
    cin  >> book_name;

    cout << "Enter author name :: ";
    cin  >> author;

    cout << "Enter book-id     :: ";
    cin  >> book_id;

    cout << "Price             :: ";
    cin  >> price;
}

string book::get_book_id(){
    return book_id;
}

void book::print_details(){
    cout << "Book_name   :: "<<book_name<<"\n";
    cout << "Author name :: "<<author<<"\n";
    cout << "Book-id     :: "<<book_id<<"\n";
    cout << "Price       :: "<<price<<"\n";
}

class book_list{
    struct book_Mod{
        book bk;
        vector<pair<int,bool>> serial_num;
    };
    vector<book_Mod> book_det;
    int check_duplicate(string);
public:
    bool is_available(string);
    void add_book(book);
    int issue_book(string);
    void return_book(string, int);
    void print_details();
};

int book_list::check_duplicate(string book_id){
    for(int i=0; i<book_det.size();i++){
        if(book_det[i].bk.get_book_id() == book_id){
            return i;
        }
    }
    return -1;
}

bool book_list::is_available(string book_id){
    if(check_duplicate(book_id) == -1)
        return false;
    else return true;
}

void book_list::add_book(book b){
    int book_Index = check_duplicate(b.get_book_id());
    if(book_Index == -1){
        book_Mod b_mod;
        b_mod.bk = b;
        pair<int,bool> temp(b_mod.serial_num.size() + 1,true);
        b_mod.serial_num.push_back(temp);
        book_det.push_back(b_mod);
    } else{
        pair<int,bool> temp(book_det[book_Index].serial_num.size()+1, true);
        book_det[book_Index].serial_num.push_back(temp);
    }
}

int book_list::issue_book(string book_id){
    int book_Index = check_duplicate(book_id);
    if(book_Index == -1){
        cout << "Book not Found !! \n";
        return 0;
    } else{
        for(int i=0; i<book_det[book_Index].serial_num.size(); i++){
            if(i<book_det[book_Index].serial_num[i].second == true){
                book_det[book_Index].serial_num[i].second = false;
                return book_det[book_Index].serial_num[i].first;
            }
        };
        cout << "Book not available !! \n";
        return 0;
    }
}

void book_list::return_book(string book_id, int sl_no){
    int book_Index = check_duplicate(book_id);
    book_det[book_Index].serial_num[sl_no-1].second = true;
}

void book_list::print_details(){
    for(int i=0;i<book_det.size(); i++){
        book_det[i].bk.print_details();
        int count_available = 0;
        for(int j=0; j<book_det[i].serial_num.size(); i++)
            if(book_det[i].serial_num[i].second == true)
                count_available++;
        cout << "Available copies = "<<count_available<<"\n\n";
    }
}


class MemberList{
    struct book_det{
        string book_id;
        int sl_no;
        int transaction_no;
    };
    struct m_list{
        string mail_id;
        string mem_name;
        string mem_id;
        int num_books;
        int num_books_taken;
        book_det* transac_det;
    };
    vector<m_list> mem_list;
public:
    bool IsALreadyRegistered(string mem_name, string mail_id);
    string registration(string mem_name, string mail_id, char flag);
    bool IsEligibleFor(string mem_id);
    void IssuedBook(string mem_id, string book_id, int sl, int transac_id);
    bool IsBookTakenBy(string mem_id, string book_id, int sl);
    void ReturnedBook(string mem_id, string book_id, int sl);
    void print_details(string mem_name);
};

bool MemberList::IsALreadyRegistered(string mem_name, string mail_id){
    for(int i=0;i<mem_list.size(); i++){
        if(mem_list[i].mem_name == mem_name && mem_list[i].mail_id == mail_id)
            return true;
    }
    return false;
}

string MemberList::registration(string mem_name, string mail_id, char flag){
    static int count = 0;
    if(IsALreadyRegistered(mem_name, mail_id))
        return "";
    else{
        string mem_id = to_string(++count);
        mem_id = mem_id + flag;
        m_list temp;
        temp.mail_id = mail_id;
        temp.mem_name = mem_name;
        switch(flag){
            case 's':
                temp.num_books = 2;
                break;
            case 'f':
                temp.num_books = 10;
                break;
            default:
                cout <<"Not a valid flag";
                return "";
        }
        temp.num_books_taken = 0;
        temp.transac_det = new book_det[temp.num_books];
        return mem_id;
    }
}

bool MemberList::IsEligibleFor(string mem_id){
    for(int i=0; i<mem_list.size(); i++){
        if(mem_list[i].mem_id == mem_id){
            if(mem_list[i].num_books_taken < mem_list[i].num_books)
                return true;
        }
    }
    return false;
}

void MemberList::IssuedBook(string mem_id, string book_id, int sl, int transac_id){
    for(int i=0; i<mem_list.size(); i++){
        if(mem_list[i].mem_id == mem_id){
            m_list &temp = mem_list[i];
            temp.num_books_taken++;
            for(int j=0; j<temp.num_books; j++){
                if(temp.transac_det[i].transaction_no != 0){
                    temp.transac_det[i].book_id = book_id;
                    temp.transac_det[i].sl_no = sl;
                    temp.transac_det[i].transaction_no = transac_id;
                    break;
                }
            }
            return;
        }
    }
}

bool MemberList::IsBookTakenBy(string mem_id, string book_id, int sl){
    for(int i=0; i<mem_list.size(); i++){
        
    }
}

class Members{
protected:
    string name;
    string mail_id;
    string ph_no;
    string mem_id;
public:
    virtual void getdata() = 0;
    virtual void register_mem() = 0;
    virtual void issue_book() = 0;
    virtual void return_book() = 0;
    virtual void print_det() = 0;
};

class Student:public Members{
public:
    void getdata();
    void register_mem();
    void issue_book();
    void return_book();
    void print_det();
};
