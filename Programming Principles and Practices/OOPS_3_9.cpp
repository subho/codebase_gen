#include <iostream>
using namespace std;

class elem{
    int em;
    elem* next;
public:
    elem();
    friend class STACK;
};

elem::elem(){
    em = 0;
    next = NULL;
}

class STACK{
    elem* head;
public:
    STACK();
    void push(int);
    int pop();
    ~STACK();
};

STACK::STACK(){
    head = NULL;
}

void STACK::push(int i){
    elem* temp = new elem();
    if(temp == NULL){
        cout<<"overflow error \n";
        return;
    }
    temp->em = i;
    temp->next = head;
    head = temp;

    return;
}

int STACK::pop(){
    if(head == NULL){
        cout<<"Underflow error !!\n";
        return 0;
    }
    elem* temp = head;
    head = head->next;
    int tmp = temp->em;
    delete temp;
    return tmp;
}

STACK::~STACK(){
    while(head != NULL){
        elem* temp = head;
        head = head->next;
        delete temp;
    }
}

int main(){
    int choice;
    STACK S;

    cout<<": Press 1 to push     :\n"
        <<": Press 2 to pop      :\n"
        <<": Press 3 to stop     :\n";

    do{
        cout<<"Express choice :: ";
        cin >> choice;

        if(choice == 1){
            int num;
            cout<<"Enter the number :: ";
            cin >> num;
            S.push(num);
        }
        else if(choice == 2)
            cout <<"Popped element is :: "<<S.pop()<<"\n";
        else if(choice == 3){
            cout<<"Terminating the process :-D \n";
            break;
        }
        else cout<<"Wrong choice. Continuing... \n";
    }while(choice != 3);

    return 0;
}
