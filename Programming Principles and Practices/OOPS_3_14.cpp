#include <iostream>
#include <string>
using namespace std;

class d{
    int year;
    int month;
    int day;
public:
    d();
    void update_date();
    void show_date();
};

class BALANCE{
    string ACC_NO;
    d date;
    float balance;
public:
    BALANCE(string="\0",float=0);
    void set_details();
    void update_balance(int = 0);
    float ret_balance();
    void check_balance();

};

d::d(){
    year  = -1 ;
    month = -1 ;
    day   = -1 ;
}

void d::update_date(){
    cout<<"Enter Date in this format d/m/y for date/n";

    cin >> day;
    while(day>31 || day < 1){
        cout<<"wrong date!! Enter again\n";
        cin>>day;
    }

    char c;
    cin >>c;
    cin>>month;
    while(month>12 || month < 1){
        cout<<"wrong month!! Enter again\n";
        cin>>month;
    }

    cin>>c;
    cin>>year;
    while(year < 0){
        cout<<"wrong year!! Enter again\n";
        cin>>year;
    }
}

void d::show_date(){
    if(day == -1){
        cout<<"Till now, data is not entered \n";
        return;
    }
    cout<<"Date is :: ";
    cout<<day<<"/"<<month<<"/"<<year<<"\n";

    return;
}

BALANCE::BALANCE(string acc_no,float blnce){
    ACC_NO  = acc_no;
    balance = blnce;
}

void BALANCE::set_details(){
    cout<<"Enter the Account no. here :: ";
    cin >> ACC_NO;
}

void BALANCE::update_balance(int blnce){
    if(blnce == 0){
    cout<<"Enter the balance here :: ";
    cin >> balance;
    }
    else balance = blnce;

    cout<<"Now, enter the data :: \n";
    date.update_date();
}

float BALANCE::ret_balance(){
    return balance;
}

void BALANCE::check_balance(){
    cout<<"Account number :: "<<ACC_NO<<"\n";
    cout<<"Balance = "<<balance<<"\n";
    date.show_date();
    return;
}

class TRANSACTION{
    BALANCE b;
public:
    TRANSACTION();
    void do_business();
};

TRANSACTION::TRANSACTION(){
    b.set_details();
    b.update_balance();
}

void TRANSACTION::do_business(){
    float money;
    cout<<"Enter your choice :: \n";
    cout<<":Enter 1 to deposit :\n"
        <<":Enter 2 to withdraw:\n"
        <<":Enter 3 to check details:\n";
    int choice;
    cout <<"Enter your choice here :: ";
    cin >> choice;

    switch(choice){
        case 1: cout<<"Enter the amount you want to deposit :: ";
                cin >> money;
                b.update_balance(money + b.ret_balance());
                break;
        case 2: cout<<"Enter the amount to withdraw :: ";
                cin>>money;
                if(money < b.ret_balance())
                    b.update_balance(b.ret_balance()-money);
                else cout << "Bankruptcy Alert!!\n";
                break;
        case 3: b.check_balance();
                break;
        default:
            cout<<"wrong choice \n";
    }
}

int main(){
    TRANSACTION t;
    t.do_business();
    t.do_business();

    return 0;
}
