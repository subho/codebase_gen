#include <stdio.h>
#include <stdlib.h>

#define li long int

int main()
{
	int rand_seed;
	rand_seed = &rand_seed;
	srand(rand_seed);

	FILE *file;
	file = fopen("RandStr.txt","a+");

	for(int i=0; i<100000; i++)
	{
		li rand_int;
		rand_int = rand();
		char str[2];

		while(rand_int<100000000000)
		{
			rand_int = rand_int*rand();
		}

		for(int i=0;i<2;i++){
			str[i]=(rand_int%100)%26+'A';
			rand_int = rand_int/10;
		}

		for(int i=0;i<2;i++)
		{
			fprintf(file,"%c",str[i]);
		}
		fprintf(file,"\n");	
	}
	fclose(file);
	return 0;
}
