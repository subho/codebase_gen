#include <stdio.h>
#include <stdlib.h>

#define MAX_BITS 5

struct created_by{
    int index;
    struct created_by* next;
};

struct minterm{
    int bits;
    int used;
    struct created_by* elements;
    struct minterm* next;
};

void initiate_terms(struct minterm* term){
    term->bits = 0;
    term->used = 0;
    term->elements = NULL;
    term->next = NULL;
    return;
}

/* Here we need a deep copying of elements*/

void add_elems(struct created_by* elements, struct created_by** term_elems){
    if(*term_elems == NULL){
        struct created_by* temp;
        temp = (struct created_by*)malloc(sizeof(struct created_by));

        temp->index = elements->index;
        temp->next = NULL;
        *term_elems = temp;

        elements = elements->next;
    }

    struct created_by* curr = *term_elems;

    while(curr->next != NULL)
        curr = curr->next;

    while(elements!=NULL){
        struct created_by* temp;
        temp = (struct created_by*)malloc(sizeof(struct created_by));

        temp->index = elements->index;
        temp->next = elements->next;
        curr->next = temp;

        curr = curr->next;
        elements = elements->next;
    }

    return;
}

struct minterm* create(struct created_by* elements, int bits){
    struct minterm* term;
    term = (struct minterm*)malloc(sizeof(struct minterm));
    initiate_terms(term);

    term->bits = term->bits|bits;
    add_elems(elements,&(term->elements));
    return term;
}

void display(const struct minterm* terms){
    if(terms == NULL) return;
    while(terms!=NULL){
        int mask = 1;
        mask = mask<<(2*MAX_BITS-1);
        printf("Bits are : ");
        for(int i=0; i<2*MAX_BITS;i++){
            if((mask&terms->bits)>0)
                printf("1");
            else printf("0");
            if(i%2 !=0) printf(" ");
            mask= mask>>1;
        }

        printf("\t|\tElements are : ");

        struct created_by* elements = terms->elements;

        while(elements != NULL){
            printf("%d ",elements->index);
            elements = elements->next;
        }

        printf("\t|\tUsed? = ");

        if(terms->used == 0) printf("No\n");
        else printf("Yes\n");
        terms = terms->next;
    }
    return;
}

void del_used_elems(struct minterm** head){
    if(*head == NULL) return;
    struct minterm* curr = *head;
    while((*head)!=NULL && (*head)->used == 1){
        *head = (*head)->next;
        free(curr);
        curr = *head;
    }

    struct minterm* prev = curr;
    while(curr!=NULL){
        if(curr->used == 1){
            prev->next = curr->next;
            free(curr);
            curr = prev->next;
        }
        else{
            prev = curr;
            curr = curr->next;
        }
    }

    return;
}

void del_duplicates(struct minterm* head){
    if(head == NULL) return;
    while(head->next!=NULL){
        int dup = head->bits;
        struct minterm* curr = head->next;
        struct minterm* prev = head;
        while(curr!=NULL){
            if(curr->bits == dup){
                prev->next = curr->next;
                free(curr);
                curr = prev->next;
            }
            else{
                prev = curr;
                curr = curr->next;
            }
        }
        head = head->next;
        if(head == NULL) break;
    }

    return;
}

int merge_terms(struct minterm* term_lower, struct minterm* term_higher){
    if(term_lower == NULL || term_higher == NULL)
        return 0;

    int count = 0;

    struct minterm* new_terms = NULL;
    struct minterm* term_higher_temp = NULL;

    while(term_lower!=NULL){
        term_higher_temp = term_higher;
        while(term_higher_temp!=NULL){
            int temp;
            temp = term_lower->bits|term_higher_temp->bits;
            if(term_higher_temp->bits == temp){
                count = count+1;

                temp = (term_higher_temp->bits^term_lower->bits);
                temp=(term_higher_temp->bits)|(temp<<1);

                term_higher_temp->used  = 1;
                term_lower->used = 1;

                if(new_terms == NULL){
                    new_terms = create(term_lower->elements,temp);
                    add_elems(term_higher_temp->elements,&(new_terms->elements));
                }
                else{
                    struct minterm* temp_elem = new_terms;

                    while(temp_elem->next!=NULL)
                        temp_elem = temp_elem->next;

                    temp_elem->next = create(term_lower->elements,temp);
                    add_elems(term_higher_temp->elements,&(temp_elem->next->elements));
                }
            }
            term_higher_temp = term_higher_temp->next;
        }
        if(term_lower->next != NULL)
            term_lower = term_lower->next;
        else
            break;
    }

    if(new_terms != NULL) term_lower->next=new_terms;

    return count;
}




int main()
{
    int num_terms = 0;
    printf("Enter the number of terms :: ");
    scanf ("%d",&num_terms);

    struct minterm** terms;
    terms = (struct minterm **)malloc(sizeof(struct minterm*)*(MAX_BITS+1));

    for(int i=0; i<=MAX_BITS;i++)
        terms[i] = NULL;

    while(num_terms--)
    {
        int temp = 0;
        int temp_bit = 0;
        int count_1 = 0;
        for(int i=0;i<MAX_BITS;i++){
            temp = temp<<2;
            scanf("%d", &temp_bit);
            temp = temp_bit|temp;
            if(temp_bit == 1) count_1++;
        }

        struct created_by* elements;
        elements = (struct created_by*)malloc(sizeof(struct created_by));
        elements->index = num_terms;
        elements->next = NULL;

        if(terms[count_1]==NULL)
            terms[count_1] = create(elements,temp);
        else{
            struct minterm* temp_terms = terms[count_1];

            while(temp_terms->next!=NULL)
                temp_terms = temp_terms->next;

            temp_terms->next = create(elements,temp);
        }
    }

    for(int i=0; i<MAX_BITS+1;i++)
        display(terms[i]);

    int merged;

    do{
        merged = 0;
        for(int i=0;i<MAX_BITS;i++)
            merged += merge_terms(terms[i],terms[i+1]);

        for(int i=0;i<MAX_BITS+1;i++){
            del_used_elems(&terms[i]);
            del_duplicates(terms[i]);
        }

        printf("\n++++++++++++++++++++++++++++++++++\n");
        for(int i=0; i<MAX_BITS+1; i++)
            display(terms[i]);
        printf("++++++++++++++++++++++++++++++++++\n");
    }while(merged>0);

    return 0;
}
