#include <stdio.h>
#include <stdlib.h>
#include <sys/types.h> //getpid
#include <unistd.h> //for getpid
#include <sys/wait.h> //for waiting

int i=0;

void doSomeWork(char* who){
    printf(" I'm the ya know %s\n", who);
    const int NUM_TIMES = 5;
        for(; i< NUM_TIMES; i++){
            sleep(rand()%4);
            printf("Done pass %s :: %d\n",who,i);
        }
}

int main(){
    sleep(5);
    pid_t pid = fork();
    printf("fork returned :: %d\n",pid);
    if(pid<0)
        perror("Fork failes");
    if(pid == 0){
        printf("I am the child process with pid %d\n", getpid());
        sleep(5);
        doSomeWork("child");
        printf("Child exiting\n");
        exit(6);
    }
    //We must be parent

    printf("I'm the parent, waiting for child to end.\n");
    sleep(30);
    //doSomeWork("parent");
    int status = 0;
    //pid_t childpid = wait(&status);
    printf("Parent knows child finished with status %d \n", status);
    int childReturnValue = WEXITSTATUS(status);
    printf("Return value was %d\n", childReturnValue);
    /*We know the child process when fork returns 0*/

    printf("getpid :: %d\n",getpid());
    return 0;
}
