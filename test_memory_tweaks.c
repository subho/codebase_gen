#include <stdio.h>
#include <stdlib.h>

#define TRUE 1
#define FALSE 0

typedef struct block_tag{
    struct block_tag* next;
    unsigned short int is_free;
    int size;
    void* mem_address;
} block;

block* MEM = NULL;
block* FREE_MEM = NULL;

void* mem_alloc(int size){
    if(MEM == NULL){
        MEM = (block *)malloc(sizeof(block)+size);
        MEM->is_free = TRUE;
        MEM->size    = sizeof(MEM->is_free);
        printf("%d\n",MEM->size);
        MEM->mem_address = (char *)MEM+sizeof(block);
        MEM->next = NULL;
        return MEM->mem_address;
    }
    else{

    }
}

int main(){
    int* p = mem_alloc(sizeof(int));
    *p = 12;
    printf("%d\n", sizeof(MEM));
    printf("%d\n",MEM->is_free);
    for(int i=0; i<MEM->size;i++){
        printf("%d.%d \n",i+1,*((char *)MEM + i));
    }
    printf("%d\n",*p);
}
