#include <stdio.h>

int main()
{
	int x;
	int *p;
	int** pp;
	
	//here p is pointing to x
	p = &x;
	
	//here pp is pointing to p
	pp = &p;
	
	printf("Enter the value of x:");
	scanf("%d[^\n]", &x);
	
	//the value of x is
	printf("%d\n", x);
	
	//with the help of pointer
	printf("%d\n", *p);
	
	//with the help of pointer to pointer
	printf("%d\n", **pp);
	
	return 0;
}	

	
		
