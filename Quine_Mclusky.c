#include <stdio.h>
#include  <stdlib.h>

#define MAX_BITS 4
#define DEBUG 0

int max_elements;

struct minterm{
    unsigned int bits : 2*MAX_BITS;
    int* arr;
    unsigned int used : 2;
    struct minterm* next;
};

void initializer(struct minterm* inputs)
{
    inputs->bits = 0;

    max_elements = 1;
    for(int i=0;i<MAX_BITS;i++)
        max_elements *= 2;
    inputs->arr = (int *)calloc(sizeof(int),max_elements);

    inputs->used = 0;
    inputs->next = NULL;
    return;
}

void merge_arr(int* arr1, int* arr2)
{
    if(DEBUG) printf("Entered merge_arr\n");

    for(int i=0;i<max_elements;i++)
        arr1[i] = arr1[i]|arr2[i];

    if(DEBUG) printf("Exited merge_arr\n");
    return;
}

void create(int* num, int temp,struct minterm** term)
{
    if(DEBUG) printf("Entered create\n");

    struct minterm* min;
    min = (struct minterm*)malloc(sizeof(struct minterm));

    initializer(min);
    min->bits = min->bits | temp;

    if(DEBUG) printf("%d\n",min->bits);

    for(int i=0;i<max_elements;i++)
        (min->arr)[i]=(min->arr)[i]|num[i];

    *term = min;

    if(DEBUG) printf("Exited create\n");

    return;
}



void append(int* num, int temp,struct minterm* term)
{
    if(DEBUG) printf("Entered append\n");

    struct minterm* min;
    min = (struct minterm*)malloc(sizeof(struct minterm));

    initializer(min);
    min->bits = min->bits | temp;

    if(DEBUG) printf("%d\n",min->bits);

    for(int i=0;i<max_elements;i++)
        (min->arr)[i]=(min->arr)[i]|num[i];

    while(term->next!=NULL)
        term = term->next;
    term->next= min;

    if(DEBUG) printf("Exited append\n");

    return;
}

void display(const struct minterm* term)
{
    if(DEBUG) printf("Entered Display\n");

    while(term!=NULL){
        int mask = 1;
        mask = mask<<(2*MAX_BITS-1);
        for(int i=0;i<=2*MAX_BITS-1;i++){
            if(mask&term->bits) printf("1");
            else printf("0");
            mask = mask>>1;
            if(i%2!=0) printf(" ");
        }

        printf("\t|\t");

        for(int i=0;i<max_elements;i++)
            if(term->arr[i] == 1)
                printf("%d  ",i);

        printf("\t|\t");

        printf("%d\n",term->used);
        term = term->next;
    }

    if(DEBUG) printf("Exited Display\n");
    return;
}

int value(int i)
{
    if(i == 0) return 0;
    while((i&1) == 0)
        i = i>>1;
    return i;
}

int merge_list(struct minterm *lower, struct minterm *higher)
{
    if(DEBUG) printf("Entered merge_list\n");

    int i=0;
    struct minterm* new_list = NULL;

    while(lower !=NULL){
        while(higher!=NULL){
            int val = value(higher->bits^lower->bits);
            if(val == 1 || val == 3 || val == 2)
            {
                int temp;
                temp = higher->bits;
                lower->used = lower->used|1;
                higher->used = higher->used|1;
                merge_arr(lower->arr, higher->arr);
                if(val == 1)
                    temp = temp|(lower->bits^higher->bits)<<1;
                else if(val == 3 || val == 2) temp=higher->bits|lower->bits;
                if(i==0) create(lower->arr, temp, &new_list);
                else append(lower->arr, temp, new_list);
                i++;
            }
            higher = higher->next;
        }
        if(lower->next == NULL)
            break;
        else lower = lower->next;
    }

    lower->next = new_list;

    if(DEBUG) display(new_list);
    if(DEBUG) printf("Exited merge_list\n");

    if(i>0) return 1;
    else return 0;
}

void swap_contents(struct minterm* elem1, struct minterm* elem2)
{
    if(DEBUG) printf("Entered swap_contents\n");
    struct minterm temp;

    temp.bits = elem1->bits;
    temp.used = elem1->used;
    temp.arr = elem1->arr;

    elem1->bits = elem2->bits;
    elem1->used = elem2->used;
    elem1->arr = elem2->arr;

    elem2->bits = temp.bits;
    elem2->used = temp.used;
    elem2->arr = temp.arr;

    if(DEBUG) printf("Exited swap_contents\n");
    return;
}

void sort(struct minterm* elements)
{
    if(DEBUG)  printf("Entered sort\n");
    struct minterm* ptr1;
    struct minterm* ptr2;
    ptr1 = elements;

    while(ptr1->next!=NULL){
        ptr2 = ptr1->next;
        do{
            if((ptr1->bits)>(ptr2->bits))
                swap_contents(ptr1,ptr2);
            ptr2 = ptr2->next;
        }
        while(ptr2!=NULL);
        ptr1 = ptr1->next;
    }
    if(DEBUG) display(elements);
    if(DEBUG) printf("Exited sort\n");
    return;
}

void del_used_elem(struct minterm** head)
{
    if(DEBUG) printf("Entered del_used_elem\n");

    while((*head)!=NULL && (*head)->used == 1){
        struct minterm* p;
        p  = *head;
        *head = (*head)->next;
        free(p);
    }

    struct minterm *curr, *prev;
    prev=*head;
    if((*head)!=NULL && (*head)->next!=NULL)
        curr=(*head)->next;
    else curr = NULL;

    while(curr!=NULL)
    {
        if(curr->used == 1){
            prev->next = curr->next;
            free(curr);
            curr = prev->next;
        }
        else {
            prev = curr;
            curr = curr->next;
        }
    }
    if(DEBUG) display(*head);
    if(DEBUG) printf("Exited del_used_elem\n");
    return;
}

void del_duplicates(struct minterm* head)
{
    if(DEBUG) printf("Entered del_duplicates\n");

    struct minterm *curr, *prev;
    prev = head;
    curr = head->next;
    int temp = prev->bits;
    while(curr!=NULL)
    {
        if(curr->bits==temp){
            prev->next = curr->next;
            free(curr);
            curr = prev->next;
        }
        else{
            prev = curr;
            temp = prev->bits;
            curr = curr->next;
        }
    }
    if(DEBUG) display(head);
    if(DEBUG) printf("Exited del_duplicates\n");
    return;
}

void merge_internals(struct minterm* terms)
{
    struct minterm* loop1 = terms;
    struct minterm* loop2;
    while(loop1->next != NULL){
        loop2 = loop1->next;
        while(loop2 != NULL){
            int val = value(loop1->bits^loop2->bits);
            if(val ==2 || val ==3)
            {
                loop2->bits = loop1->bits|loop2->bits;
                loop1->bits = loop2->bits;
            }
            loop2 = loop2->next;
        }
        loop1 = loop1->next;
    }
    return ;
}

int main()
{
    int n;
    printf("Enter the number of elements :: ");
    scanf("%d",&n);

    struct minterm** terms;
    terms = (struct minterm**)calloc(sizeof(struct minterm *),(MAX_BITS+1));

    for(int i=0;i<=MAX_BITS;i++)
        terms[i]=NULL;

    for(int i=0;i<n;i++){
        int temp = 0;
        int x;
        int count = 0;
        for(int j=0;j<MAX_BITS;j++){
            temp = temp<<2;
            scanf("%d",&x);
            if(x == 1){
                count++;
                temp = temp|1;
            }
        }
        if(DEBUG)
            printf("%d\n", temp);

        int* arr;
        arr = (int *)calloc(sizeof(int),max_elements);

        arr[i] = 1;

        if(terms[count]==NULL)
            create(arr,temp,&terms[count]);
        else append(arr,temp,terms[count]);
    }

    if(DEBUG){
        for(int i=0;i<=MAX_BITS;i++)
            display(terms[i]);
    }

    int merge_op;
    do{
        merge_op = 0;
        for(int i=0;i<MAX_BITS;i++)
        if(terms[i] != NULL && terms[i+1] != NULL)
            merge_op+=merge_list(terms[i], terms[i+1]);

        for(int i=0;i<=MAX_BITS;i++){
            if(terms[i] != NULL){
                sort(terms[i]);
                del_duplicates(terms[i]);
                del_used_elem(&terms[i]);
                if(DEBUG) display(terms[i]);
            }
        }
    }while(merge_op>0);


    for(int i=0;i<=MAX_BITS;i++){
        if(terms[i]!=NULL){
            display(terms[i]);
            printf("\n");
        }

    }

    return 0;

}
