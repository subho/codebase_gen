#include <stdio.h>
#include <stdlib.h>

#define SENTINEL_INDEX -1
#define MAX_TERMS      20
/*coeff -> 0 if the term does not exist
  index -> can be anything you want*/

struct term{
    float coeff;
    int index;
};

/*num_term   -> number of terms to be included
                in a Polynomial
  last_index -> index from where space is free
  terms      -> array containing the Polynomial terms */

struct Polynomial{
  int num_term;
  struct term terms[2*MAX_TERMS];
};

/*create -> Create a Polynomial with the desired number of terms,
            Also, initialize all the terms to 0*/

struct Polynomial create()
{
  struct Polynomial p;
  p.num_term = 0;
  for(int i=0;i<2*MAX_TERMS;i++){
    p.terms[i].index = SENTINEL_INDEX;
    p.terms[i].coeff = 0.0;
  }
  return p;
}

/* Add terms to a polynomial*/

int add_term(struct Polynomial* p,float coeff, int index)
{
  if(p->num_term < MAX_TERMS && index>SENTINEL_INDEX){
    (p->terms)[p->num_term].coeff = coeff;
    (p->terms)[p->num_term].index = index;
    p->num_term++;
    return 1;
  }
  else return 0;
}

void print_polynomial(struct Polynomial p)
{
    for(int i=0;i<p.num_term;i++){
        if(p.terms[i].index!=0)
            printf("%fx^(%d)+ ",p.terms[i].coeff,p.terms[i].index);
        else printf("%f\n",p.terms[i].coeff);
    }
    return;
}

void sort_polynomial(struct Polynomial* p)
{
  for(int i=0;i< (p->num_term);i++){
    for(int j=i+1;j<p->num_term;j++){
      if((p->terms[i]).index < (p->terms[j]).index){
        struct term t;
        t = p->terms[i];
        p->terms[i] = p->terms[j];
        p->terms[j] = p->terms[i];
      }
    }
  }
  return;
}

struct Polynomial mul(struct Polynomial P1, struct Polynomial P2)
{
  struct Polynomial p;
  p = create();

  for(int i=0; i<P1.num_term; i++){
    for(int j=0; j<P2.num_term; j++){
        if(p.num_term == 2*MAX_TERMS){
            printf("Multiplication lead to buffer overflow");
            exit(1);
        }
        p.terms[p.num_term].coeff = P1.terms[i].coeff*P2.terms[j].coeff;
        p.terms[p.num_term].index = P1.terms[i].index + P2.terms[j].index;
        p.num_term++;
    }
  }

  sort_polynomial(&p);

  for(int i=0; i<p.num_term-1; i++){
    if(p.terms[i].index == p.terms[i+1].index){
        p.terms[i+1].index += p.terms[i].index;
        p.terms[i].index = SENTINEL_INDEX;
        p.num_term--;
    }
  }

  sort_polynomial(&p);

  return p;
}
