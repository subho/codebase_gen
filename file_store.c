#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#define MAX_ROLL 100

struct student{
	int roll;
	char name[31];
	int sub1;
	int sub2;
	int sub3;
	int sub4;
	int sub5;
};

void initiate_struct(struct student* s)
{
	s->roll = 0;
	strcpy(s->name,"");
	s->sub1 = 0;
	s->sub2 = 0;
	s->sub3 = 0;
	s->sub4 = 0;
	s->sub5 = 0;
}

int f_find(FILE *file, int roll)
{
	if(roll == 0 || roll > MAX_ROLL){
		printf("Wrong Roll\n");
		return 0;
	}

	struct student temp_store;
	fseek(file,(roll)*sizeof(struct student),SEEK_SET);
	fread(&temp_store,sizeof(struct student),1,file);

	if(temp_store.roll == roll)
		return 1;
	else return 0;
}

void del_data(FILE* file, int roll)
{
	struct student s;
	int called=1;
	if(f_find(file, roll)){
		printf("Deleting Data");
		fseek(file,roll*sizeof(struct student),SEEK_SET);
		fread(&s,sizeof(struct student),1,file);
		s.roll = 0;
		fseek(file,roll*sizeof(struct student),SEEK_SET);
		fwrite(&s,sizeof(struct student),1,file);
	}
	else printf("Record not found\n");

	return;
}

void get_std_data(FILE* file)
{
	static int called = 1;

	struct student s;

	printf("%d . Enter Student Roll: ", called++);
	scanf("%d",&(s.roll));

	if(s.roll > MAX_ROLL || s.roll == 0){
		printf("Error: Wrong Roll Number");
		fclose(file);
		exit(1);
	}

	if(f_find(file, s.roll)){
		printf("Record Already exists \n");
		printf("Do you want to replace it? press y or n:");
		char choice;
		scanf("%c", &choice);
		scanf("%c", &choice);
		if(choice == 'y' || choice == 'Y'){
			printf("Overwriting existing record ::\n");
		}
		else if(choice == 'n' || choice == 'N'){
			printf("\nContinuing to next...\n\n");
			return;
		}
	}

	printf("Enter Student name: ");
	scanf ("%s",s.name);

	printf("Enter marks of subject1: ");
	scanf ("%d",&(s.sub1));

	printf("Enter marks of subject2: ");
	scanf ("%d",&(s.sub2));

	printf("Enter marks of subject3: ");
	scanf ("%d",&(s.sub3));

	printf("Enter marks of subject4: ");
	scanf ("%d",&(s.sub4));

	printf("Enter marks of subject5: ");
	scanf ("%d",&(s.sub5));

	fseek(file,(s.roll)*sizeof(struct student),SEEK_SET);
	fwrite(&s,sizeof(struct student),1,file);

	printf("\n++++++++++++++++++++++++++++++++++++++++++\n");

	return;
}

void print_file(FILE* file)
{
	struct student s;
	fseek(file,0,SEEK_SET);
	while(!feof(file)){
		fread(&s,sizeof(struct student),1,file);
		if(s.roll !=0){
			printf("\n++++++++++++++++++++++++++++++++++++++++++\n");
			printf("Roll of student       : %d\n", s.roll);
			printf("Name of student       : %s\n", s.name);
			printf("Marks for sub1        : %d\n", s.sub1);
			printf("Marks for sub2        : %d\n", s.sub2);
			printf("Marks for sub3        : %d\n", s.sub3);
			printf("Marks for sub4        : %d\n", s.sub4);
			printf("Marks for sub5        : %d\n", s.sub5);
			printf("Total marks achieved  : %d\n", s.sub1+s.sub2+s.sub3+s.sub4+s.sub5);
			printf("\n++++++++++++++++++++++++++++++++++++++++++\n");
		}
	}
	return;
}

void del_record(FILE* file)
{
	int roll = 0;
	printf("Enter the roll number:: ");
	scanf("%d", &roll);

	if(f_find(file, roll)){
		fseek(file,roll*sizeof(struct student),SEEK_SET);
		roll = 0;
		fwrite(&roll,sizeof(int),1,file);
	}
	return ;
}
	


int main()
{
	FILE* file;
	file = fopen("Student_dat.txt","r+");	

	if(file == NULL){
		printf("File does not exist, creating file named \"Student_dat.txt\"\n");
		file = fopen("Student_dat.txt","w+");
		struct student s_initializer;
		initiate_struct(&s_initializer);

		for(int i=0;i<=MAX_ROLL;i++)
			fwrite(&s_initializer,sizeof(struct student),1,file);
	}

	int num_add_rec = 0;
	printf("Enter the number additions/modifications you want to make :: ");
	scanf("%d", &num_add_rec);
	
	while(num_add_rec-->0)
		get_std_data(file);

	int num_del_rec = 0;
	printf("Enter the number of deletions you want to make :: ");
	scanf("%d", &num_del_rec);

	print_file(file);
	
	while(num_del_rec-->0)
		del_record(file);

	print_file(file);

	fclose(file);
	return 0;
}
