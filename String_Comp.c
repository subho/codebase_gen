#include <stdio.h>
#include <stdlib.h>

int * stable_sort(char *arr,int num)
{
    int *count_pos;
    count_pos = (int *)malloc(27*sizeof(int));

    int count[27] = {0};

    for(int i=0; i<num; i++)
    {
        if((arr[i]-'a')<26)
            count[arr[i]-'a']++;
        else
            count[26]++;
    }

    count_pos[0] =0;
    for(int i=1; i<27; i++){
        count_pos[i] = count_pos[i-1]+count[i-1];
    }

    return count_pos;
}

int main()
{
    int num;
    printf("Enter the numbers of names: " );
    scanf("%d", &num);

    char** strings;
    strings = (char**)malloc(num*sizeof(char*));

    for(int i=0;i<num;i++)
        strings[i] = (char*)malloc(20*sizeof(char));

    for(int i=0; i<num; i++){

        for(int k=0 ;k<20 ; k++)
            strings[i][k] = '~';

        char string[20];
        scanf("%s[^\n]", string);

        for(int j=0; string[j]!='\0';j++){
            strings[i][j] = string[j];
        }
    }

    char** strings_temp;
    strings_temp = (char**)malloc(num*sizeof(char*));

    for(int i=0;i<num;i++)
        strings_temp[i] = (char*)malloc(20*sizeof(char));

    for(int i=19;i>=0;i--){
        char *arr;
        arr = (char *)malloc(sizeof(char)*num);

        for(int j=0; j<num; j++)
            arr[j] = strings[j][i];

        int *count;
        count = stable_sort(arr,num);

        for(int k=0; k<num; k++){
            int char_num;
            if(strings[k][i]-'a'<26)
                char_num = strings[k][i]-'a';
            else
                char_num = 26;
            strings_temp[count[char_num]++] = strings[k];
        }

        for(int l=0; l<num; l++)
        {
            strings[l] = strings_temp[l];
        }
    }

    for(int i=0; i<num; i++)
        printf("%s\n",strings[i]);
    return 0;

}
