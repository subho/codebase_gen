#include <stdio.h>
#include <stdlib.h>

#define ROW 5

int main()
{
	//implementing array of pointers
	//here we consider rows to be fixed
	int* arr[ROW];
	int col;
	
	printf("Enter the number of columns: ");
	scanf("%d[^\n]", &col);
	
	for(int i=0;i<ROW;i++)
		arr[i] = (int *)malloc(ROW*sizeof(int));
	
	//assigning values in arr
	for(int i=0;i<ROW;i++)
		for(int j=0;j<col;j++)
			arr[i][j] = i+j*j;
			
	//printing the values
	for(int i=0;i<ROW;i++){
		for(int j=0;j<col;j++)
			printf("%d\t",arr[i][j]);
		printf("\n");
	}
	
	return 0;
}
			
			
