#include <stdio.h>
#include <stdlib.h>

#define COL 5

int main()
{
	//specify that p is a pointer to array
	//such that the array size will be
	//=5*int
	int (*arr)[COL];
	int row;
	
	printf("Enter the number of rows: ");
	scanf("%d[^\n]",&row);
	
	for(int i=0;i<row;i++)
		arr=(int **)malloc(sizeof(int)*row*COL);
		
	//assigning values in arr	
	for(int i=0;i<row;i++)
		for(int j=0;j<COL;j++)
			arr[i][j] = i+j*j;
	
	//printing the values
	for(int i=0;i<row;i++){
		for(int j=0;j<COL;j++)
			printf("%d\t",arr[i][j]);
		printf("\n");
	}
	
	return 0;
}
	
	
	

